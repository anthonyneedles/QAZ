/*
 * clock.h
 *
 * This module handles initial clock configuration by setting the system clock
 * to the HSI48. This clock configuration is intended to be placed BEFORE main
 * project execution.
 *
 * SYSCLK = 48MHz
 *
 * Project: QAZ
 * MCU: STM32F042C6T6
 * Copyright: (c) 2020 by Anthony Needles
 * License: GNU GPL v3 (see LICENSE)
 */

#ifndef __CLOCK_H
#define __CLOCK_H

/*
 * ClockInit()
 *
 * Brief:  Enable and switch system clock over to HSI48. This function is called
 *         in "startup_stm32f042x6.S" BEFORE main() is called and normal program
 *         functionality is started.
 *
 * Params: None
 *
 * Return: None
 */
extern void ClockInit(void);

#endif /* __CLOCK_H */
