/*
 * debug_uart.h
 *
 *
 * Project: QAZ
 * MCU: STM32F042C6T6
 * Copyright: (c) 2020 by Anthony Needles
 * License: GNU GPL v3 (see LICENSE)
 */

#ifndef __DEBUG_UART_H
#define __DEBUG_UART_H

// Maximum characters that can be displayed with one DbgPrint call
#define MAX_STR_LEN (120)

/*
 * DebugUartInit()
 *
 * Brief:  When DEBUG=1, enables USART1 (and corresponding pins) for UART
 *         transmission at 115200 baud.
 *
 * Params: None
 *
 * Return: None
 */
void DebugUartInit(void);

/*
 * DbgPrint()
 *
 * Brief:  When DEBUG, this function will expand the given format string with
 *         the variable args. The final expanded string will then be output on
 *         USART1. The maximum number of characters that can be printed out with
 *         one expanded string is MAX_STR_LEN. If an expansion would result in
 *         an 'overrun' of this maximum none of the argument is printed.
 *
 *         Format specifiers:
 *         %x - Hex
 *         %c - Character
 *         %s - String
 *         %p - Pointer
 *
 *         '0n' can be placed in between the '%' and type specifier to specify
 *         the minimum width of the result. The output is zero-padded up to 'n'
 *         characters. This can only be used with 'x' and 'p' format types. Max
 *         width of 8.
 *
 *         For example %08x with an arg of 0x1234 results in 00001234
 *
 * Params: fmt - Format string, with or without format specifiers
 *         ... - Variable arguments for format speifiers
 *
 * Return: None
 */
void DbgPrintf(const char *fmt, ...);

#endif /* __DEBUG_UART_H */
