/*
 * hb.h
 *
 * Initializes heartbeat. Flashes heartbeat LED at 1Hz to provide sanity
 * check that program is not hanging and/or violating timeslice scheduling.
 *
 * Project: QAZ
 * MCU: STM32F042C6T6
 * Copyright: (c) 2020 by Anthony Needles
 * License: GNU GPL v3 (see LICENSE)
 */

#ifndef __HB_H
#define __HB_H

// LED period = 2*HB_TASK_PERIOD_MS, so LED flashes at 1Hz
#define HB_TASK_PERIOD_MS    (500)

/*
 * HeartbeatInit()
 *
 * Brief:  Intializes heartbeat LED port.
 *
 * Params: None
 *
 * Return: None
 */
void HeartbeatInit(void);

/*
 * HeartbeatTask()
 *
 * Brief: Task enters every HB_TASK_PERIOD_LOOPS to flash the hearbeat LED at
 *        a period of 2*HB_TASK_PERIOD_MS.
 *
 * Params: None
 *
 * Return: None
 */
void HeartbeatTask(void);

#endif /* __HB_H */
