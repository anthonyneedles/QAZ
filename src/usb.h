/*
 * usb.h
 *
 * This module interfaces with the USB peripheral to establish basic USB HID
 * communications with host.
 *
 * Endpoint 0 -> Control
 * Endpoint 1 -> Interrupt, TX only
 *
 * Project: QAZ
 * MCU: STM32F042K6
 * Copyright: (c) 2020 by Anthony Needles
 * License: GNU GPL v3 (see LICENSE)
 */

#ifndef __USB_H
#define __USB_H

#include "main.h"

#define USB_TASK_PERIOD_MS (5)

// Total number of EPs used
#define NUM_EP (2)

// Buffer descriptor table offset in PMA
#define BDT_OFFSET (0x0000U)

// Mask for getting # of bytes received in packet
#define USB_CNT_RX_MSK (0x03FF)

// EP0 BDT entry (TX_SIZE is filled right before transmission)
#define TX0_ADDR (0x0080U)
#define RX0_ADDR (0x0100U)
#define RX0_CNT  (0x8400U)  // BL_SIZE = 1, NUM_BLOCK = 2, Total size = 64 bytes

// EP1 BDT entry (TX only)
#define TX1_ADDR (0x0180U)

// SETUP packet bmRequestType[7]
#define REQ_DIR_IN   (0x80U)
#define REQ_DIR_OUT  (0x00U)

// SETUP packet bmRequestType[6:5]
#define REQ_TYP_STD  (0x00U)
#define REQ_TYP_CLS  (0x20U)
#define REQ_TYP_VDR  (0x40U)

// SETUP packet bmRequestType[4:0]
#define REQ_RCP_DEV  (0x00U)
#define REQ_RCP_ITF  (0x01U)
#define REQ_RCP_EP   (0x02U)

// SETUP packet bRequest
#define REQ_GET_DESC (0x06U)
#define REQ_SET_ADDR (0x05U)
#define REQ_SET_CFG  (0x09U)
#define REQ_SET_IDLE (0x0AU)
#define REQ_SET_RPT  (0x09U)

// Entire bmRequestType field
#define REQ_IN_STD_DEV  (REQ_DIR_IN  | REQ_TYP_STD | REQ_RCP_DEV)
#define REQ_IN_STD_ITF  (REQ_DIR_IN  | REQ_TYP_STD | REQ_RCP_ITF)
#define REQ_OUT_CLS_ITF (REQ_DIR_OUT | REQ_TYP_CLS | REQ_RCP_ITF)
#define REQ_OUT_STD_DEV (REQ_DIR_OUT | REQ_TYP_STD | REQ_RCP_DEV)

// Obtains endpoint register for ep identifier 'epn'
#define EP_REG(epn) (*((&USB->EP0R) + (epn << 2U)))

// Combines SETUP packet bRequest/bmRequestType fields
#define REQ(type, req) (((uint16_t)(type) << 8) | ((uint16_t)(req) & 0xFF))

#define SET_TX_STATUS(epn, status) \
    (EP_REG(epn) = (EP_REG(epn) ^ (status & USB_EPTX_STAT)) \
        & (USB_EPREG_MASK | USB_EPTX_STAT));

#define SET_RX_STATUS(epn, status) \
    (EP_REG(epn) = (EP_REG(epn) ^ (status & USB_EPRX_STAT)) \
        & (USB_EPREG_MASK | USB_EPRX_STAT));

// SETUP packet as it will appear in memory
typedef struct __PACKED {
    uint8_t bmRequestType;
    uint8_t bRequest;
    uint16_t wValue;
    uint16_t wIndex;
    uint16_t wLength;
} usb_setup_packet_t;

// Buffer descriptor table entry as it will appear in memory
typedef struct __PACKED {
    uint16_t tx_addr;
    uint16_t tx_size;
    uint16_t rx_addr;
    uint16_t rx_size;
} buf_desc_t;

// Entire buffer descriptor table
typedef struct {
    buf_desc_t bd_ep[NUM_EP];
} buf_desc_table_t;

/*
 * USBInit()
 *
 * Brief:  Performs USB port, clock, and peripheral initialization. Since the
 *         USB ports are used in startup (for DFU), only the NOE signal port is
 *         initialized. Host detects device upon DP pullup enable.
 *
 * Params: None
 *
 * Return: None
 */
void USBInit(void);

void USBTask(void);

/*
 * USB_IRQHandler()
 *
 * Brief:  Routes module function based on received interrupts. Most are
 *         ignored, except RESET and CTR.
 *
 * Params: None
 *
 * Return: None
 */
void USB_IRQHandler(void);

#endif /* __USB_H */
