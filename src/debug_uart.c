/*
 * debug_uart.c
 *
 *
 * Project: QAZ
 * MCU: STM32F042C6T6
 * Copyright: (c) 2020 by Anthony Needles
 * License: GNU GPL v3 (see LICENSE)
 */

#include "debug_uart.h"
#include "main.h"

#include <stdarg.h>

#if defined(DEBUG)

// Is there a '0n' after the '%'?
#define IS_WIDTH_SPECIFIER(x,y) ((x == '0') && (y >= '0' && y <= '8'))

// Expand %x format specifier
static bool expand_x(int val, char *str, int *str_idx, int width);

// Expand %x format specifier
static bool expand_s(char *s, char *str, int *str_idx);

/*
 * DebugUartInit() - Public function
 *
 * Brief:  When DEBUG, enables USART1 (and corresponding pins) for UART
 *         transmission at 115200 baud. PA9 = TX, PA10 = RX. Only using
 *         TX for now.
 *
 * Params: None
 *
 * Return: None
 */
void DebugUartInit(void)
{
    // Enable clock for GPIOA and USART1
    RCC->AHBENR  |= RCC_AHBENR_GPIOAEN;
    RCC->APB2ENR |= RCC_APB2ENR_USART1EN;

    // Set debug tx port into alt function mode
    GPIOA->MODER = ((GPIOA->MODER & ~GPIO_MODER_MODER9_Msk)
            | GPIO_MODER_MODER9_1);

    // Set alternate function 1 (USART1_TX)
    GPIOA->AFR[1] = ((GPIOA->AFR[1] & ~GPIO_AFRH_AFSEL9_Msk)
            | (1UL << GPIO_AFRH_AFSEL9_Pos));

    // Set pullup
    GPIOA->PUPDR = ((GPIOA->PUPDR  & ~GPIO_PUPDR_PUPDR9_Msk)
            | GPIO_PUPDR_PUPDR9_0);

    // Fast output
    GPIOA->OSPEEDR = ((GPIOA->OSPEEDR  & ~GPIO_OSPEEDR_OSPEEDR9_Msk)
            | GPIO_OSPEEDR_OSPEEDR9);

    // Enable transmitting
    USART1->CR1 = USART_CR1_TE;

    // Set baudrate to 115200
    USART1->BRR = 0x1A1;

    // Enable USART1
    USART1->CR1 |= USART_CR1_UE;

    DbgPrintf("\n");
}

/*
 * DbgPrint() - Public function (that breaks my naming convention, oh well)
 *
 * Brief:  When DEBUG, this function will expand the given format string with
 *         the variable args. The final expanded string will then be output on
 *         USART1. The maximum number of characters that can be printed out with
 *         one expanded string is MAX_STR_LEN. If an expansion would result in
 *         an 'overrun' of this maximum none of the argument is printed.
 *
 *         Format specifiers:
 *         %x - Hex
 *         %c - Character
 *         %s - String
 *         %p - Pointer
 *
 *         '0n' can be placed in between the '%' and type specifier to specify
 *         the minimum width of the result. The output is zero-padded up to 'n'
 *         characters. This can only be used with 'x' and 'p' format types. Max
 *         width of 8.
 *
 *         For example %08x with an arg of 0x1234 results in 00001234
 *
 * Params: fmt - Format string, with or without format specifiers
 *         ... - Variable arguments for format speifiers
 *
 * Return: None
 */
void DbgPrintf(const char *fmt, ...)
{
    va_list args;
    va_start(args, fmt);

    int width;

    // Set to true if expansion would result in MAX_STR_LEN overrun
    bool overrun = false;

    // Expanded string buffer
    char str[MAX_STR_LEN];

    // Indicies of our input format/expanded string
    int fmt_idx = 0;
    int str_idx = 0;

    // Go through the fmt string one character at a time
    while ((fmt[fmt_idx] != '\0') && (str_idx < MAX_STR_LEN) && !overrun) {
        width = 0;

        // Format specifier prefix found
        if (fmt[fmt_idx] == '%') {
            fmt_idx++;

            if (IS_WIDTH_SPECIFIER(fmt[fmt_idx], fmt[fmt_idx + 1])) {
                width = fmt[fmt_idx + 1] - '0';
                fmt_idx += 2;
            }

            switch (fmt[fmt_idx]) {
            case 'x':
                // Hex format specifier
                overrun = expand_x(va_arg(args, int), &str[0], &str_idx, width);
                fmt_idx++;
                break;
            case 'c':
                // Character format speicifer
                str[str_idx++] = (char)va_arg(args, int);
                fmt_idx++;
                break;
            case 's':
                // String format speicifer
                overrun = expand_s(va_arg(args, char *), &str[0], &str_idx);
                fmt_idx++;
                break;
            case 'p':
                // Pointer format speicifer
                overrun = expand_x(va_arg(args, int), &str[0], &str_idx, width);
                fmt_idx++;
                break;
            case '\0':
                // Don't print a null
                break;
            default:
                // Print any other character
                str[str_idx++] = fmt[fmt_idx++];
                break;
            }
        } else if (fmt[fmt_idx] == '\n'){
            // Add a carriage return before every newline
            str[str_idx++] = '\r';
            str[str_idx++] = fmt[fmt_idx++];
        } else {
            // Just a good ol character
            str[str_idx++] = fmt[fmt_idx++];
        }
    }

    // str is fully expanded, now print the whole thing out
    for (int i = 0; i < str_idx; ++i) {
        while ((USART1->ISR & USART_ISR_TXE) == 0);
        USART1->TDR = str[i];
    }
}

/*
 * expand_x() - Private function
 *
 * Brief:  Expands a %x format specifer argument as a hex value
 *         interpreted string of characters (no NULL termination).
 *
 * Params: val     - Argument to convert to string of characters
 *         str     - Expanded string buffer
 *         str_idx - Current index of expanded string buffer
 *         width   - 0 padded width of number
 *
 * Return: false if expansion would not result in MAX_STR_LEN overrun
 *         true  if expansion would     result in MAX_STR_LEN overrun
 */
static bool expand_x(int val, char *str, int *str_idx, int width) {
    char buf[MAX_STR_LEN];
    unsigned int digit;
    unsigned int uval;
    int buf_idx = 0;
    bool overrun = false;

    uval = (unsigned int)(val);

    // Convert digits to their hex characters, in reverse order
    while (uval >= 16) {
        digit = uval % 16;
        uval /= 16;
        if (digit < 10) {
            buf[buf_idx++] = digit + '0';
        } else {
            buf[buf_idx++] = (digit - 10) + 'A';
        }
    }

    // Convert the first digit
    if (uval < 10) {
        buf[buf_idx++] = uval + '0';
    } else {
        buf[buf_idx++] = (uval - 10) + 'A';
    }

    while (buf_idx < width) {
        buf[buf_idx++] = '0';
    }
    // Now that we know the whole size, detect overrun
    if ((*str_idx + buf_idx) > MAX_STR_LEN) {
        overrun = true;
    }

    // Copy temp buffer to expanded str buffer
    while (buf_idx > 0) {
        str[(*str_idx)++] = buf[--buf_idx];
    }

    return overrun;
}

/*
 * expand_s() - Private function
 *
 * Brief:  Expands a %s format specifer argument as a string.
 *
 * Params: s       - String argument
 *         str     - Expanded string buffer
 *         str_idx - Current index of expanded string buffer
 *
 * Return: false if expansion would not result in MAX_STR_LEN overrun
 *         true  if expansion would result in MAX_STR_LEN overrun
 */
static bool expand_s(char *s, char *str, int *str_idx) {
    int s_idx = 0;
    bool overrun = false;

    // We need to find the size of the string argument
    while (s[s_idx] != '\0') {
        s_idx++;
    }

    // Now that we know the whole size, detect overrun
    if ((*str_idx + s_idx) > MAX_STR_LEN) {
        overrun = true;
    }

    // Copy string arument to expanded str buffer
    s_idx = 0;
    while (s[s_idx] != '\0') {
        str[(*str_idx)++] = s[s_idx++];
    }

    return overrun;
}

#else

/*
 * Does nothing when not DEBUG
 */
void DebugUartInit(void) {
}

/*
 * Does nothing when not DEBUG
 */
void DbgPrintf(const char *fmt, ...) {
    UNUSED(fmt);
}

#endif
