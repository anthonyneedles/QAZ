/*
 * systick.c
 *
 * Handles all SysTick initialization and functionality for time slice
 * preemptive multitasking. Dependent on 48MHz Cortex system timer.
 *
 * Project: QAZ
 * MCU: STM32F042C6T6
 * Copyright: (c) 2020 by Anthony Needles
 * License: GNU GPL v3 (see LICENSE)
 */

#include "systick.h"
#include "main.h"

#define CLKCYCLES_PER_MS 48000U

static volatile int systickCurrentMSCount = 0U;
static int systickLastCount = 0U;
static int systickInitFlag = 0U;

/*
 * SysTickInit
 *
 * Brief:  Enables SysTick timer via CMSIS SysTick_Config (found in
 *         arch/core_cm0.h) with required clock cycles to result in 1ms
 *         interrupts.
 *
 * Params: None
 *
 * Return: None
 */
void SysTickInit(void)
{
    uint32_t st_error = SysTick_Config(CLKCYCLES_PER_MS);

    if (st_error == 0U) {
		    // 0U = Systick timer successfully loaded
		    systickInitFlag = 1;

        DbgPrintf("SysTick Wait Task:\n\r");
        DbgPrintf("  Selected loop period: 0x%x ms\n\r", LOOP_PERIOD_MS);
	  } else {
		    // 1U = Reload value impossible
		    systickInitFlag = 0;

        DbgPrintf("SysTick load error!\n\r");
	  }
}

/*
 * SysTickWaitTask
 *
 * Brief:  Upon first time pass saves current millisecond count value. Every
 *         following call will result in program idle until next time slice
 *         period.
 *
 * Params: ts_period - Desired time slice period in ms
 *
 * Return: None
 */
void SysTickWaitTask(void)
{
    int loop_elapsed_ms;

    if (systickInitFlag == 1U) {
        loop_elapsed_ms = systickCurrentMSCount - systickLastCount;

        if (loop_elapsed_ms > LOOP_PERIOD_MS) {
            // We overran the loop period, we got a problem
            DbgPrintf("ERROR: overran loop period by %xms\n\r",
                    loop_elapsed_ms - LOOP_PERIOD_MS);
        }

        // Wait for the loop period to finish
        while ((systickCurrentMSCount - systickLastCount) < LOOP_PERIOD_MS) {}

        systickLastCount = systickCurrentMSCount;
    } else {
        // Failure in init, we got a problem
        DbgPrintf("SysTick Init Failure\n\r");
        while (1) {}
    }
}

/*
 * SysTickHandler
 *
 * Brief:  Interrupts will occur at 1 kHz (every millisecond). Increments
 *         millisecond count every entrance.
 *
 * Params: None
 *
 * Return: None
 */
void SysTick_Handler(void)
{
	systickCurrentMSCount++;
}
