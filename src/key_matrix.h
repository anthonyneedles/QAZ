/*
 * key_matrix.h
 *
 * This module handles initializing and scanning all columns/rows in key matrix.
 *
 * Project: QAZ
 * MCU: STM32F042C6T6
 * Copyright: (c) 2020 by Anthony Needles
 * License: GNU GPL v3 (see LICENSE)
 */

#ifndef __KEY_MATRIX_H
#define __KEY_MATRIX_H

#include "main.h"

// Scans keys every 10ms
#define SCAN_TASK_PERIOD_MS (50)

#define PORT(num, port) {num, GPIO##port}

#if defined(QAZ) /* QAZ ----------------------------------------------------- */

#define NUM_ROWS (5)
#define NUM_COLS (15)

#define ROW_PORTS \
    PORT(14, B), PORT(13, B), PORT( 4, B), PORT( 7, B), PORT( 9, B)

#define COL_PORTS \
    PORT(15, B), PORT( 3, B), PORT( 5, B), PORT( 6, B), PORT( 8, B), \
    PORT(13, C), PORT(15, C), PORT( 4, A), PORT( 0, A), PORT( 5, A), \
    PORT(14, C), PORT( 1, A), PORT( 2, A), PORT( 3, A), PORT( 6, A)

#define BASE_KEYS \
    ROW(ESCAPE,     1,     2,     3,     4,     5,     6,     7,     8,      9,      0,     DASH,   EQUALS, BSPACE, GRAVE)    \
    ROW(   TAB,     q,     w,     e,     r,     t,     y,     u,     i,      o,      p, LBRACKET, RBRACKET, BSLASH, DELETE)   \
    ROW(  CAPS,     a,     s,     d,     f,     g,     h,     j,     k,      l,   SEMI,    QUOTE,    UNDEF, RETURN, PAGEUP)   \
    ROW( LSHFT,     z,     x,     c,     v,     b,     n,     m, COMMA, PERIOD, FSLASH,    UNDEF,    RSHFT, UARROW, PAGEDOWN) \
    ROW( LCTRL,  LGUI,  LALT, UNDEF, UNDEF, SPACE, UNDEF, UNDEF,  UNDEF,  RALT,  RCTRL,       FN,   LARROW, DARROW, RARROW)

#define FN_KEYS \
    ROW( UNDEF,    F1,    F2,    F3,    F4,    F5,    F6,    F7,    F8,     F9,    F10,      F11,      F12,  UNDEF, UNDEF)    \
    ROW( UNDEF, UNDEF, UNDEF, UNDEF, UNDEF, UNDEF, UNDEF, UNDEF, UNDEF,  UNDEF,  UNDEF,    UNDEF,    UNDEF,  UNDEF, INSERT)   \
    ROW( UNDEF, UNDEF, UNDEF, UNDEF, UNDEF, UNDEF, UNDEF, UNDEF, UNDEF,  UNDEF,  UNDEF,    UNDEF,    UNDEF,  UNDEF, PAGEUP)   \
    ROW( UNDEF, UNDEF, UNDEF, UNDEF, UNDEF, UNDEF, UNDEF, UNDEF, UNDEF,  UNDEF,  UNDEF,    UNDEF,    UNDEF,  UNDEF, PAGEDOWN) \
    ROW( UNDEF, UNDEF, UNDEF, UNDEF, UNDEF, UNDEF, UNDEF, UNDEF, UNDEF,  UNDEF,  UNDEF,       FN,    UNDEF,  UNDEF, UNDEF)

#define KEY(x) ((key_t)(HID_USAGE_KEYBOARD_##x))

#define ROW(_1, _2, _3, _4, _5, _6, _7, _8, _9, _10, _11, _12, _13, _14, _15) \
    { KEY(_1),  KEY(_2),  KEY(_3),  KEY(_4),  KEY(_5),  \
      KEY(_6),  KEY(_7),  KEY(_8),  KEY(_9),  KEY(_10), \
      KEY(_11), KEY(_12), KEY(_13), KEY(_14), KEY(_15) },

#elif defined(QAZ_NUMPAD) /* QAZ NUMPAD ------------------------------------- */

#define NUM_ROWS (5)
#define NUM_COLS (4)

#define ROW_PORTS \
    PORT( 8, B), PORT( 7, A), PORT( 5, A), PORT( 6, A), PORT( 4, A)

#define COL_PORTS \
    PORT( 0, B), PORT(14, B), PORT(12, B), PORT( 9, B)

#define BASE_KEYS \
    ROW(NUMLCK, FSLASH, ASTRSK, DASH)  \
    ROW(     7,      8,      9, UNDEF) \
    ROW(     4,      5,      6, PLUS)  \
    ROW(     1,      2,      3, UNDEF) \
    ROW(     0,  UNDEF, PERIOD, ENTER)

#define FN_KEYS \
    ROW( UNDEF,  UNDEF,  UNDEF, UNDEF) \
    ROW( UNDEF,  UNDEF,  UNDEF, UNDEF) \
    ROW( UNDEF,  UNDEF,  UNDEF, UNDEF) \
    ROW( UNDEF,  UNDEF,  UNDEF, UNDEF) \
    ROW( UNDEF,  UNDEF,  UNDEF, UNDEF)

#define KEY(x) ((key_t)(HID_USAGE_KEYPAD_##x))

#define ROW(_1, _2, _3, _4) \
    { KEY(_1),  KEY(_2),  KEY(_3),  KEY(_4) },

#else
    #error No valid hardware defined!
#endif

typedef uint16_t key_t;

typedef struct key_layers {
    key_t base;
    key_t   fn;
} key_layers_t;

/*
 * KeyMatrixInit()
 *
 * Brief:  Configures all rows as inputs with pullups and columns as open-drain
 *         outputs.
 *
 * Params: None
 *
 * Return: None
 */
void KeyMatrixInit(void);

/*
 * KeyMatrixScanTask()
 *
 * Brief:  Performs key matrix scanning every SCAN_PERIOD task loops.
 *
 * Params: None
 *
 * Return: None
 */
void KeyMatrixScanTask(void);

#endif /* __KEY_MATRIX_H */
