/*
 * hb.c
 *
 * Initializes heartbeat. Flashes heartbeat LED at 1Hz to provide sanity
 * check that program is not hanging and/or violating timeslice scheduling.
 *
 * Project: QAZ
 * MCU: STM32F042C6T6
 * Copyright: (c) 2020 by Anthony Needles
 * License: GNU GPL v3 (see LICENSE)
 */

#include "hb.h"
#include "main.h"

#define HB_TASK_PERIOD_LOOPS (HB_TASK_PERIOD_MS/LOOP_PERIOD_MS)

// HB driving GPIO port
#define HB_LED_NUM  (12)
#define HB_LED_PORT (GPIOB)

#define HB_LED_ON()  (HB_LED_PORT->ODR |=  (1UL << HB_LED_NUM))
#define HB_LED_OFF() (HB_LED_PORT->ODR &= ~(1UL << HB_LED_NUM))

#define OUTPUT (1UL)

/*
 * HeartbeatInit()
 *
 * Brief:  Intializes heartbeat LED drive.
 *
 * Params: None
 *
 * Return: None
 */
void HeartbeatInit(void)
{
    DbgPrintf("LED Hearbeat Task:\n\r");
    DbgPrintf("  Attempted task period: 0x%x ms\n\r", HB_TASK_PERIOD_MS);
    DbgPrintf("  Real task period: 0x%x ms (0x%x * 0x%x)\n\r",
            HB_TASK_PERIOD_LOOPS*LOOP_PERIOD_MS,
            HB_TASK_PERIOD_LOOPS, LOOP_PERIOD_MS);

    // Enabble HB LED GPIO port clock
    RCC->AHBENR |= RCC_AHBENR_GPIOBEN;

    // Set HB LED as GPIO output
    HB_LED_PORT->MODER = ((HB_LED_PORT->MODER & ~(0x3UL << (HB_LED_NUM * 2)))
            | (OUTPUT << (HB_LED_NUM * 2)));
}

/*
 * HeartbeatTask()
 *
 * Brief: Task enters every HB_TASK_PERIOD_LOOPS to flash the hearbeat LED at
 *        a period of 2*HB_TASK_PERIOD_MS.
 *
 * Params: None
 *
 * Return: None
 */
void HeartbeatTask(void)
{
    static int task_cnt = HB_TASK_PERIOD_LOOPS;
    static int state = 0;

    if (task_cnt >= HB_TASK_PERIOD_LOOPS && HB_TASK_PERIOD_LOOPS != 0) {
        task_cnt = 0;
        state = !state;

        if (state) {
            HB_LED_ON();
        } else {
            HB_LED_OFF();
        }
    }

    task_cnt++;
}
