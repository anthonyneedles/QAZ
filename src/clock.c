/*
 * clock.c
 *
 * This module handles initial clock configuration by setting the system clock
 * to the HSI48. This clock configuration is intended to be placed BEFORE main
 * project execution.
 *
 * SYSCLK = 48MHz
 *
 * Project: QAZ
 * MCU: STM32F042C6T6
 * Copyright: (c) 2020 by Anthony Needles
 * License: GNU GPL v3 (see LICENSE)
 */

#include "clock.h"
#include "main.h"

/*
 * ClockInit()
 *
 * Brief:  Enable and switch system clock over to either HSI48 or external
 *         crystal through pll. This function is called in
 *         "startup_stm32f042x6.S" BEFORE main() is called and normal program
 *         functionality is started.
 *
 * Params: None
 *
 * Return: None
 */
void ClockInit(void)
{
#if defined(EXT_CRYSTAL)

    // Enable Clock Security System and High Speed External clock
    RCC->CR |= (RCC_CR_CSSON | RCC_CR_HSEON);

    // Wait until HSE oscillator is stable (~512 HSE clock pulses)
    while ((RCC->CR & RCC_CR_HSERDY_Msk) != RCC_CR_HSERDY) {}

    // Set system clock mux to HSE (SYSCLK is now 8MHz)
    RCC->CFGR = ((RCC->CFGR & ~RCC_CFGR_SW_Msk) | RCC_CFGR_SW_HSE);

    // Turn PLL off to change parameters
    RCC->CR &= ~RCC_CR_PLLON;

    // Wait until PLL is unlocked (off)
    while ((RCC->CR & RCC_CR_PLLRDY_Msk) == RCC_CR_PLLRDY) {}

    // Set PLL multiplication factor to 6 (6 x 8Mhz = 48MHz) MUST NOT EXCEED 6!
    // Select HSE as PLL source
    RCC->CFGR = ((RCC->CFGR & ~(RCC_CFGR_PLLMUL_Msk | RCC_CFGR_PLLSRC_Msk))
            | RCC_CFGR_PLLMUL6
            | RCC_CFGR_PLLSRC_HSE_PREDIV);

    // Turn PLL on as parameters are changed
    RCC->CR |= RCC_CR_PLLON;

    // Wait until PLL is locked (on)
    while ((RCC->CR & RCC_CR_PLLRDY_Msk) != RCC_CR_PLLRDY){}

    // Set system clock mux to PLL (SYSCLK is now 48MHz)
    RCC->CFGR = ((RCC->CFGR & ~RCC_CFGR_SW_Msk) | RCC_CFGR_SW_PLL);

#elif defined(HSI_48)

    // Enable HSI48 clock
    RCC->CR2 |= RCC_CR2_HSI48ON;

    // Wait for HSI48 to stabilize
    while ((RCC->CR2 & RCC_CR2_HSI48RDY_Msk) != RCC_CR2_HSI48RDY) {}

    // Ensure APB and AHB prescalers are correct
    RCC->CFGR = ((RCC->CFGR & ~(RCC_CFGR_PPRE_Msk | RCC_CFGR_HPRE_Msk))
            | RCC_CFGR_PPRE_DIV1
            | RCC_CFGR_HPRE_DIV1);

    // Switch system clock to HSI48
    RCC->CFGR = ((RCC->CFGR & ~RCC_CFGR_SW_Msk) | RCC_CFGR_SW_HSI48);

    // Wait for system clock to switch to HSI48
    while ((RCC->CFGR & RCC_CFGR_SWS_Msk) != RCC_CFGR_SWS_HSI48) {}

#else
    #error No valid clock source defined!
#endif

    // Select SYSCLK as MCO output
    RCC->CFGR = ((RCC->CFGR & ~RCC_CFGR_MCO_Msk) | RCC_CFGR_MCO_SYSCLK);

    // Enable GPIOA clock (MCO pin used is PA8)
	  RCC->AHBENR |= RCC_AHBENR_GPIOAEN;

    GPIOA->MODER |= GPIO_MODER_MODER8_0;

	  // Set MCO pin to alternate function
	  GPIOA->MODER = ((GPIOA->MODER & ~GPIO_MODER_MODER8_Msk)
            | (2UL << GPIO_MODER_MODER8_Pos));

    // Set for high speed
    GPIOA->OSPEEDR = ((GPIOA->OSPEEDR & ~GPIO_OSPEEDR_OSPEEDR8_Msk)
            | (3UL << GPIO_OSPEEDR_OSPEEDR8_Pos));

	  // Set MCO pin to alternate function 0 (MCO)
	  GPIOA->AFR[1] &= ~GPIO_AFRH_AFSEL8_Msk;
}
