/*
 * systick.h
 *
 * Handles all SysTick initialization and functionality for time slice
 * preemptive multitasking. Dependent on 48MHz Cortex system timer.
 *
 * Project: QAZ
 * MCU: STM32F042C6T6
 * Copyright: (c) 2020 by Anthony Needles
 * License: GNU GPL v3 (see LICENSE)
 */

#ifndef SYSTICK_H_
#define SYSTICK_H_

#define LOOP_PERIOD_MS (5)

/*
 * SysTickInit
 *
 * Brief:  Enables SysTick timer via CMSIS SysTick_Config (found in
 *         arch/core_cm0.h) with required clock cycles to result in 1ms
 *         interrupts.
 *
 * Params: None
 *
 * Return: None
 */
void SysTickInit(void);

/*
 * SysTickWaitTask
 *
 * Brief:  Upon first time pass saves current millisecond count value. Every
 *         following call will result in program idle until next time slice
 *         period.
 *
 * Params: None
 *
 * Return: None
 */
void SysTickWaitTask(void);

/*
 * SysTickHandler
 *
 * Brief:  Interrupts will occur at 1 kHz (every millisecond). Increments
 *         millisecond count every entrance.
 *
 * Params: None
 *
 * Return: None
 */
void SysTick_Handler(void);

#endif /* SYSTICK_H_ */
