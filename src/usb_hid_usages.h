/*
 * usb_hid_usages.h
 *
 *
 * Project: QAZ
 * MCU: STM32F042K6
 * Copyright: (c) 2020 by Anthony Needles
 * License: GNU GPL v3 (see LICENSE)
 */

#ifndef __USB_HID_USAGES_H
#define __USB_HID_USAGES_H

#include "main.h"

// Keyboard/Keypad Page (0x07)

// User-defined (not sent)
#define HID_USAGE_KEYBOARD_FN       (0xFF)

#define HID_USAGE_KEYPAD_FN         (HID_USAGE_KEYBOARD_FN)

// Errors
#define HID_USAGE_KEYBOARD_NOEVENT  (0x00)
#define HID_USAGE_KEYBOARD_ROLLOVER (0x01)
#define HID_USAGE_KEYBOARD_POSTFAIL (0x02)
#define HID_USAGE_KEYBOARD_UNDEF    (0x03)

#define HID_USAGE_KEYPAD_NOEVENT    (HID_USAGE_KEYBOARD_NOEVENT)
#define HID_USAGE_KEYPAD_ROLLOVER   (HID_USAGE_KEYBOARD_ROLLOVER)
#define HID_USAGE_KEYPAD_POSTFAIL   (HID_USAGE_KEYBOARD_POSTFAIL)
#define HID_USAGE_KEYPAD_UNDEF      (HID_USAGE_KEYBOARD_UNDEF)

// Letters
#define HID_USAGE_KEYBOARD_a        (0x04)
#define HID_USAGE_KEYBOARD_b        (0x05)
#define HID_USAGE_KEYBOARD_c        (0x06)
#define HID_USAGE_KEYBOARD_d        (0x07)
#define HID_USAGE_KEYBOARD_e        (0x08)
#define HID_USAGE_KEYBOARD_f        (0x09)
#define HID_USAGE_KEYBOARD_g        (0x0A)
#define HID_USAGE_KEYBOARD_h        (0x0B)
#define HID_USAGE_KEYBOARD_i        (0x0C)
#define HID_USAGE_KEYBOARD_j        (0x0D)
#define HID_USAGE_KEYBOARD_k        (0x0E)
#define HID_USAGE_KEYBOARD_l        (0x0F)
#define HID_USAGE_KEYBOARD_m        (0x10)
#define HID_USAGE_KEYBOARD_n        (0x11)
#define HID_USAGE_KEYBOARD_o        (0x12)
#define HID_USAGE_KEYBOARD_p        (0x13)
#define HID_USAGE_KEYBOARD_q        (0x14)
#define HID_USAGE_KEYBOARD_r        (0x15)
#define HID_USAGE_KEYBOARD_s        (0x16)
#define HID_USAGE_KEYBOARD_t        (0x17)
#define HID_USAGE_KEYBOARD_u        (0x18)
#define HID_USAGE_KEYBOARD_v        (0x19)
#define HID_USAGE_KEYBOARD_w        (0x1A)
#define HID_USAGE_KEYBOARD_x        (0x1B)
#define HID_USAGE_KEYBOARD_y        (0x1C)
#define HID_USAGE_KEYBOARD_z        (0x1D)

// Numbers
#define HID_USAGE_KEYBOARD_1        (0x1E)
#define HID_USAGE_KEYBOARD_2        (0x1F)
#define HID_USAGE_KEYBOARD_3        (0x20)
#define HID_USAGE_KEYBOARD_4        (0x21)
#define HID_USAGE_KEYBOARD_5        (0x22)
#define HID_USAGE_KEYBOARD_6        (0x23)
#define HID_USAGE_KEYBOARD_7        (0x24)
#define HID_USAGE_KEYBOARD_8        (0x25)
#define HID_USAGE_KEYBOARD_9        (0x26)
#define HID_USAGE_KEYBOARD_0        (0x27)

// Symbols
#define HID_USAGE_KEYBOARD_DASH     (0x2D)
#define HID_USAGE_KEYBOARD_EQUALS   (0x2E)
#define HID_USAGE_KEYBOARD_LBRACKET (0x2F)
#define HID_USAGE_KEYBOARD_RBRACKET (0x30)
#define HID_USAGE_KEYBOARD_BSLASH   (0x31)
#define HID_USAGE_KEYBOARD_SEMI     (0x33)
#define HID_USAGE_KEYBOARD_QUOTE    (0x34)
#define HID_USAGE_KEYBOARD_GRAVE    (0x35)
#define HID_USAGE_KEYBOARD_COMMA    (0x36)
#define HID_USAGE_KEYBOARD_PERIOD   (0x37)
#define HID_USAGE_KEYBOARD_FSLASH   (0x38)

// Modifier keys
#define HID_USAGE_KEYBOARD_CAPS     (0x39)
#define HID_USAGE_KEYBOARD_SCROLL   (0x47)
#define HID_USAGE_KEYBOARD_LCTRL    (0xE0)
#define HID_USAGE_KEYBOARD_LSHFT    (0xE1)
#define HID_USAGE_KEYBOARD_LALT     (0xE2)
#define HID_USAGE_KEYBOARD_LGUI     (0xE3)
#define HID_USAGE_KEYBOARD_RCTRL    (0xE4)
#define HID_USAGE_KEYBOARD_RSHFT    (0xE5)
#define HID_USAGE_KEYBOARD_RALT     (0xE6)
#define HID_USAGE_KEYBOARD_RGUI     (0xE7)

// Keypad Keys
#define HID_USAGE_KEYPAD_NUMLCK     (0x53)
#define HID_USAGE_KEYPAD_FSLASH     (0x54)
#define HID_USAGE_KEYPAD_ASTRSK     (0x55)
#define HID_USAGE_KEYPAD_DASH       (0x56)
#define HID_USAGE_KEYPAD_PLUS       (0x57)
#define HID_USAGE_KEYPAD_ENTER      (0x58)
#define HID_USAGE_KEYPAD_1          (0x59)
#define HID_USAGE_KEYPAD_2          (0x5A)
#define HID_USAGE_KEYPAD_3          (0x5B)
#define HID_USAGE_KEYPAD_4          (0x5C)
#define HID_USAGE_KEYPAD_5          (0x5D)
#define HID_USAGE_KEYPAD_6          (0x5E)
#define HID_USAGE_KEYPAD_7          (0x5F)
#define HID_USAGE_KEYPAD_8          (0x60)
#define HID_USAGE_KEYPAD_9          (0x61)
#define HID_USAGE_KEYPAD_0          (0x62)
#define HID_USAGE_KEYPAD_PERIOD     (0x63)

// Function keys
#define HID_USAGE_KEYBOARD_F1       (0x3A)
#define HID_USAGE_KEYBOARD_F2       (0x3B)
#define HID_USAGE_KEYBOARD_F3       (0x3C)
#define HID_USAGE_KEYBOARD_F4       (0x3D)
#define HID_USAGE_KEYBOARD_F5       (0x3E)
#define HID_USAGE_KEYBOARD_F6       (0x3F)
#define HID_USAGE_KEYBOARD_F7       (0x40)
#define HID_USAGE_KEYBOARD_F8       (0x41)
#define HID_USAGE_KEYBOARD_F9       (0x42)
#define HID_USAGE_KEYBOARD_F10      (0x43)
#define HID_USAGE_KEYBOARD_F11      (0x44)
#define HID_USAGE_KEYBOARD_F12      (0x45)

// Arrows
#define HID_USAGE_KEYBOARD_RARROW   (0x4F)
#define HID_USAGE_KEYBOARD_LARROW   (0x50)
#define HID_USAGE_KEYBOARD_DARROW   (0x51)
#define HID_USAGE_KEYBOARD_UARROW   (0x52)

// Special keys
#define HID_USAGE_KEYBOARD_RETURN   (0x28)
#define HID_USAGE_KEYBOARD_ESCAPE   (0x29)
#define HID_USAGE_KEYBOARD_BSPACE   (0x2A)
#define HID_USAGE_KEYBOARD_TAB      (0x2B)
#define HID_USAGE_KEYBOARD_SPACE    (0x2C)
#define HID_USAGE_KEYBOARD_PRINTSCR (0x46)
#define HID_USAGE_KEYBOARD_INSERT   (0x49)
#define HID_USAGE_KEYBOARD_PAGEUP   (0x4B)
#define HID_USAGE_KEYBOARD_DELETE   (0x4C)
#define HID_USAGE_KEYBOARD_PAGEDOWN (0x4E)

#endif /* __USB_HID_USAGES_H */
