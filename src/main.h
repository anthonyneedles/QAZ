/*
 * main.h
 *
 * Common includes go here.
 *
 * Project: QAZ
 * MCU: STM32F042C6T6
 * Copyright: (c) 2020 by Anthony Needles
 * License: GNU GPL v3 (see LICENSE)
 */

#ifndef __MAIN_H
#define __MAIN_H

// Needed types
#include <stdint.h>
#include <stdbool.h>

// Peripheral access
#include "../arch/stm32f042x6.h"

// Debug messages
#include "debug_uart.h"

// Loop period
#include "systick.h"

// For suppressing warnings
#define UNUSED(x) ((void)(x))

/*
 * nop_delay() - Public function
 *
 * Brief:  Will 'nop' delay based on input value.
 *
 * Params: nops - number of nops to loop over
 *
 * Return: None
 */
static inline void nop_delay(volatile unsigned nops)
{
    while(nops) {
        __NOP();
        nops--;
    }
}

#endif /* __MAIN_H */
