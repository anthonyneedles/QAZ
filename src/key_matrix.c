/*
 * key_matrix.c
 *
 * This module handles initializing and scanning all columns/rows in key matrix.
 *
 * Project: QAZ
 * MCU: STM32F042C6T6
 * Copyright: (c) 2020 by Anthony Needles
 * License: GNU GPL v3 (see LICENSE)
 */

#include "key_matrix.h"
#include "main.h"
#include "usb_hid_usages.h"

#define SCAN_TASK_PERIOD_LOOPS (SCAN_TASK_PERIOD_MS/LOOP_PERIOD_MS)

// SET/CLR does not mean driving the column 3.3V/GND, but rather (respectively)
// activating/deactivating it. Since the inputs are pulled HIGH, and the outputs
// are open drain, SET/CLR corresponds to (respectively) GND/HIGH-Z
#define SET_COL(col) ((col.port)->ODR &= ~(1UL << col.port_num))
#define CLR_COL(col) ((col.port)->ODR |=  (1UL << col.port_num))

// Row inputs are pulled HIGH, and active LOW, this will evaulate to '1' if the
// row is active (LOW) and '0' if the row is inactive (HIGH)
#define READ_ROW(row) (!((row.port)->IDR &= (1UL << row.port_num)))

// Nops between setting a column and reading the rows
#define READ_DELAY  (175)

#define OUTPUT (1UL)
#define ODRAIN (1UL)
#define INPUT  (0UL)
#define PULLUP (1UL)

#define KEY_BUF_SIZE (8)

typedef struct gpio_port {
    int port_num;
    GPIO_TypeDef *port;
} gpio_port_t;

typedef struct key_buf {
    int idx;
    key_layers_t buf[KEY_BUF_SIZE];
} key_buf_t;

static key_buf_t key_in;

// Defining what physical pins our row inputs occupy
static const gpio_port_t row[NUM_ROWS] = {
    ROW_PORTS
};

// Defining what physical pins our column outputs occupy
static const gpio_port_t col[NUM_COLS] = {
    COL_PORTS
};

// Defining the base-layer keycode matrix
static const key_t base_keys[NUM_ROWS][NUM_COLS] = {
    BASE_KEYS
};

// Defining the fn-layer keycode matrix
static const key_t fn_keys[NUM_ROWS][NUM_COLS] = {
    FN_KEYS
};

/*
 * KeyMatrixInit()
 *
 * Brief:  Configures all rows as inputs with pullups and columns as open-drain
 *         outputs.
 *
 * Params: None
 *
 * Return: None
 */
void KeyMatrixInit(void)
{
    DbgPrintf("Key Matrix Scan Task:\n\r");
    DbgPrintf("  Attempted task period: 0x%x ms\n\r", SCAN_TASK_PERIOD_MS);
    DbgPrintf("  Real task period: 0x%x ms (0x%x * 0x%x)\n\r",
            SCAN_TASK_PERIOD_LOOPS*LOOP_PERIOD_MS,
            SCAN_TASK_PERIOD_LOOPS, LOOP_PERIOD_MS);

    // Enable clocking for GPIO ports we are using
    RCC->AHBENR |= RCC_AHBENR_GPIOAEN;
    RCC->AHBENR |= RCC_AHBENR_GPIOBEN;
    RCC->AHBENR |= RCC_AHBENR_GPIOCEN;

    // Set all rows as inputs with pullups
    for (int i = 0; i < NUM_ROWS; ++i) {
        (row[i].port)->MODER &= ~(0x3UL  << (row[i].port_num * 2));
        (row[i].port)->MODER |=  (INPUT  << (row[i].port_num * 2));
        (row[i].port)->PUPDR &= ~(0x3UL  << (row[i].port_num * 2));
        (row[i].port)->PUPDR |=  (PULLUP << (row[i].port_num * 2));
    }

    // Set all columns as open-drain outputs
    for (int i = 0; i < NUM_COLS; ++i) {
        (col[i].port)->MODER  &= ~(0x3UL  << (col[i].port_num * 2));
        (col[i].port)->MODER  |=  (OUTPUT << (col[i].port_num * 2));
        (col[i].port)->OTYPER |=  (ODRAIN <<  col[i].port_num);
    }
}

/*
 * KeyMatrixScanTask()
 *
 * Brief:  Performs key matrix scanning every SCAN_TASK_PERIOD_LOOPS task loops.
 *
 * Params: None
 *
 * Return: None
 */
void KeyMatrixScanTask(void)
{
    static int task_cnt = SCAN_TASK_PERIOD_LOOPS;
    key_layers_t key;
    bool fn = false;

    if (task_cnt >= SCAN_TASK_PERIOD_LOOPS && SCAN_TASK_PERIOD_LOOPS != 0) {
        task_cnt = 0;
        key_in.idx = 0;

        // Set key buf entries to no event
        for (int i = 0; i < KEY_BUF_SIZE; ++i) {
            key_in.buf[i].base = KEY(NOEVENT);
            key_in.buf[i].fn   = KEY(NOEVENT);
        }

        // Set col, scan rows, clear col, so on...
        for (int col_num = 0; col_num < NUM_COLS; ++col_num) {
            SET_COL(col[col_num]);

            // Delay for propagation
            nop_delay(READ_DELAY);

            for (int row_num = 0; row_num < NUM_ROWS; ++row_num) {
                // Found active row
                if (READ_ROW(row[row_num])) {
                    key.base = base_keys[row_num][col_num];

                    if (key.base == KEY(FN)) {
                        fn = true;
                    } else {
                        key.fn = fn_keys[row_num][col_num];
                        key_in.buf[key_in.idx++] = key;
                        if (key_in.idx >= KEY_BUF_SIZE) {
                            break;
                        }
                    }
                }
            }

            CLR_COL(col[col_num]);
        }

        for (int i = 0; i < KEY_BUF_SIZE; ++i) {
            DbgPrintf("%02x ", fn ? key_in.buf[i].fn : key_in.buf[i].base);
        }
        DbgPrintf("\n");
    }

    task_cnt++;
}
