/*
 * Runs all initialization functions (except system clock, which is done before
 * main()), then enters a timeslice loop.
 *
 * This project firmware handles key matrix scanning and USB HID interfacing for
 * the QAZ keyboard, tracked at: https://github.com/anthonyneedles/QAZ
 *
 * Project: QAZ
 * MCU: STM32F042C6T6
 * Copyright: (c) 2020 by Anthony Needles
 * License: GNU GPL v3 (see LICENSE)
 */

#include "main.h"
#include "hb.h"
#include "key_matrix.h"
#include "systick.h"
#include "debug_uart.h"
#include "usb.h"

int main(void)
{
    DebugUartInit(); // Should keep as first init
    HeartbeatInit();
    KeyMatrixInit();
    USBInit();
    SysTickInit();
    while (1) {
        SysTickWaitTask();
        HeartbeatTask();
        KeyMatrixScanTask();
        USBTask();
    }
}
