EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 2 2
Title "QAZ Numpad Keys"
Date ""
Rev "v0.1"
Comp "Anthony Needles"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Wire Wire Line
	6750 2000 6750 2100
$Comp
L Switch:SW_Push SW17
U 1 1 5F48713E
P 6750 3300
F 0 "SW17" V 6750 3250 50  0000 R CNN
F 1 "Cherry MX" V 6700 3200 50  0001 R CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX_2.00u_Vertical_PCB" H 6750 3500 50  0001 C CNN
F 3 "~" H 6750 3500 50  0001 C CNN
	1    6750 3300
	0    -1   -1   0   
$EndComp
Wire Wire Line
	6050 3100 6750 3100
Wire Wire Line
	6750 3900 6750 3800
Text Notes 6500 3350 0    50   ~ 0
+
$Comp
L Diode:1N4148 D16
U 1 1 5F487161
P 6750 3650
F 0 "D16" V 6850 3550 50  0000 C CNN
F 1 "1N4148" V 6850 3850 50  0000 C CNN
F 2 "Diode_THT:D_DO-35_SOD27_P7.62mm_Horizontal" H 6750 3475 50  0001 C CNN
F 3 "https://assets.nexperia.com/documents/data-sheet/1N4148_1N4448.pdf" H 6750 3650 50  0001 C CNN
	1    6750 3650
	0    -1   -1   0   
$EndComp
$Comp
L Switch:SW_Push SW16
U 1 1 5F487167
P 6750 1500
F 0 "SW16" V 6750 1450 50  0000 R CNN
F 1 "Cherry MX" V 6700 1400 50  0001 R CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX_2.00u_Vertical_PCB" H 6750 1700 50  0001 C CNN
F 3 "~" H 6750 1700 50  0001 C CNN
	1    6750 1500
	0    -1   -1   0   
$EndComp
Wire Wire Line
	6050 1300 6750 1300
$Comp
L Diode:1N4148 D15
U 1 1 5F48716D
P 6750 1850
F 0 "D15" V 6850 1750 50  0000 C CNN
F 1 "1N4148" V 6850 2050 50  0000 C CNN
F 2 "Diode_THT:D_DO-35_SOD27_P7.62mm_Horizontal" H 6750 1675 50  0001 C CNN
F 3 "https://assets.nexperia.com/documents/data-sheet/1N4148_1N4448.pdf" H 6750 1850 50  0001 C CNN
	1    6750 1850
	0    -1   -1   0   
$EndComp
Wire Wire Line
	6750 2100 7050 2100
Wire Wire Line
	6750 3900 7050 3900
Connection ~ 7050 3900
Text Notes 6500 1550 0    50   ~ 0
+
Connection ~ 6050 3100
Connection ~ 6050 1300
Wire Wire Line
	6350 3900 6350 4800
Connection ~ 6350 3900
Wire Wire Line
	6050 4800 6350 4800
Text Notes 5800 4250 0    50   ~ 0
3
Wire Wire Line
	6050 4800 6050 4700
$Comp
L Switch:SW_Push SW14
U 1 1 5F4794B5
P 6050 4200
F 0 "SW14" V 6050 4150 50  0000 R CNN
F 1 "Cherry MX" V 6000 4100 50  0001 R CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX_1.00u_PCB" H 6050 4400 50  0001 C CNN
F 3 "~" H 6050 4400 50  0001 C CNN
	1    6050 4200
	0    -1   -1   0   
$EndComp
$Comp
L Diode:1N4148 D13
U 1 1 5F4794AF
P 6050 4550
F 0 "D13" V 6150 4450 50  0000 C CNN
F 1 "1N4148" V 6150 4750 50  0000 C CNN
F 2 "Diode_THT:D_DO-35_SOD27_P7.62mm_Horizontal" H 6050 4375 50  0001 C CNN
F 3 "https://assets.nexperia.com/documents/data-sheet/1N4148_1N4448.pdf" H 6050 4550 50  0001 C CNN
	1    6050 4550
	0    -1   -1   0   
$EndComp
Wire Wire Line
	5650 3900 5650 4800
Connection ~ 5650 3900
Wire Wire Line
	5350 4800 5650 4800
Text Notes 5100 4250 0    50   ~ 0
2
Wire Wire Line
	5350 4800 5350 4700
Wire Wire Line
	5350 4000 6050 4000
Connection ~ 5350 4000
$Comp
L Switch:SW_Push SW10
U 1 1 5F447C86
P 5350 4200
F 0 "SW10" V 5350 4150 50  0000 R CNN
F 1 "Cherry MX" V 5300 4100 50  0001 R CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX_1.00u_PCB" H 5350 4400 50  0001 C CNN
F 3 "~" H 5350 4400 50  0001 C CNN
	1    5350 4200
	0    -1   -1   0   
$EndComp
$Comp
L Diode:1N4148 D9
U 1 1 5F447C80
P 5350 4550
F 0 "D9" V 5450 4450 50  0000 C CNN
F 1 "1N4148" V 5450 4750 50  0000 C CNN
F 2 "Diode_THT:D_DO-35_SOD27_P7.62mm_Horizontal" H 5350 4375 50  0001 C CNN
F 3 "https://assets.nexperia.com/documents/data-sheet/1N4148_1N4448.pdf" H 5350 4550 50  0001 C CNN
	1    5350 4550
	0    -1   -1   0   
$EndComp
Wire Wire Line
	4950 3900 4950 4800
Connection ~ 4950 3900
Wire Wire Line
	4650 4800 4950 4800
Text Notes 4400 4250 0    50   ~ 0
1
Wire Wire Line
	4650 4800 4650 4700
Wire Wire Line
	4450 4000 4650 4000
Connection ~ 4650 4000
Wire Wire Line
	4650 4000 5350 4000
$Comp
L Switch:SW_Push SW5
U 1 1 5F3167CC
P 4650 4200
F 0 "SW5" V 4650 4150 50  0000 R CNN
F 1 "Cherry MX" V 4600 4100 50  0001 R CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX_1.00u_PCB" H 4650 4400 50  0001 C CNN
F 3 "~" H 4650 4400 50  0001 C CNN
	1    4650 4200
	0    -1   -1   0   
$EndComp
$Comp
L Diode:1N4148 D4
U 1 1 5F3167C6
P 4650 4550
F 0 "D4" V 4750 4450 50  0000 C CNN
F 1 "1N4148" V 4750 4750 50  0000 C CNN
F 2 "Diode_THT:D_DO-35_SOD27_P7.62mm_Horizontal" H 4650 4375 50  0001 C CNN
F 3 "https://assets.nexperia.com/documents/data-sheet/1N4148_1N4448.pdf" H 4650 4550 50  0001 C CNN
	1    4650 4550
	0    -1   -1   0   
$EndComp
Text HLabel 4450 4000 0    50   Output ~ 0
ROW03
Text HLabel 4450 1300 0    50   Output ~ 0
ROW00
Text HLabel 4450 2200 0    50   Output ~ 0
ROW01
Text HLabel 4450 3100 0    50   Output ~ 0
ROW02
Wire Wire Line
	4650 2000 4650 2100
Wire Wire Line
	4650 3000 4650 2900
$Comp
L Switch:SW_Push SW4
U 1 1 5F315CC3
P 4650 3300
F 0 "SW4" V 4650 3250 50  0000 R CNN
F 1 "Cherry MX" V 4600 3200 50  0001 R CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX_1.00u_PCB" H 4650 3500 50  0001 C CNN
F 3 "~" H 4650 3500 50  0001 C CNN
	1    4650 3300
	0    -1   -1   0   
$EndComp
Wire Wire Line
	4650 3900 4650 3800
Text Notes 4400 3350 0    50   ~ 0
4
$Comp
L Diode:1N4148 D3
U 1 1 5F315CBD
P 4650 3650
F 0 "D3" V 4750 3550 50  0000 C CNN
F 1 "1N4148" V 4750 3850 50  0000 C CNN
F 2 "Diode_THT:D_DO-35_SOD27_P7.62mm_Horizontal" H 4650 3475 50  0001 C CNN
F 3 "https://assets.nexperia.com/documents/data-sheet/1N4148_1N4448.pdf" H 4650 3650 50  0001 C CNN
	1    4650 3650
	0    -1   -1   0   
$EndComp
Text Notes 4300 1550 0    50   ~ 0
Num\nLock
$Comp
L Switch:SW_Push SW2
U 1 1 5F30B6DF
P 4650 1500
F 0 "SW2" V 4650 1450 50  0000 R CNN
F 1 "Cherry MX" V 4600 1400 50  0001 R CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX_1.00u_PCB" H 4650 1700 50  0001 C CNN
F 3 "~" H 4650 1700 50  0001 C CNN
	1    4650 1500
	0    -1   -1   0   
$EndComp
Text Notes 4400 2450 0    50   ~ 0
7
$Comp
L Diode:1N4148 D1
U 1 1 5F2DC5D8
P 4650 1850
F 0 "D1" V 4750 1750 50  0000 C CNN
F 1 "1N4148" V 4750 2050 50  0000 C CNN
F 2 "Diode_THT:D_DO-35_SOD27_P7.62mm_Horizontal" H 4650 1675 50  0001 C CNN
F 3 "https://assets.nexperia.com/documents/data-sheet/1N4148_1N4448.pdf" H 4650 1850 50  0001 C CNN
	1    4650 1850
	0    -1   -1   0   
$EndComp
$Comp
L Diode:1N4148 D2
U 1 1 5F3117C8
P 4650 2750
F 0 "D2" V 4750 2650 50  0000 C CNN
F 1 "1N4148" V 4750 2950 50  0000 C CNN
F 2 "Diode_THT:D_DO-35_SOD27_P7.62mm_Horizontal" H 4650 2575 50  0001 C CNN
F 3 "https://assets.nexperia.com/documents/data-sheet/1N4148_1N4448.pdf" H 4650 2750 50  0001 C CNN
	1    4650 2750
	0    -1   -1   0   
$EndComp
$Comp
L Switch:SW_Push SW3
U 1 1 5F3117CE
P 4650 2400
F 0 "SW3" V 4650 2350 50  0000 R CNN
F 1 "Cherry MX" V 4600 2300 50  0001 R CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX_1.00u_PCB" H 4650 2600 50  0001 C CNN
F 3 "~" H 4650 2600 50  0001 C CNN
	1    4650 2400
	0    -1   -1   0   
$EndComp
Wire Wire Line
	4650 2100 4950 2100
Wire Wire Line
	4950 2100 4950 3000
Wire Wire Line
	4650 3900 4950 3900
Wire Wire Line
	4650 3000 4950 3000
Connection ~ 4950 3000
Wire Wire Line
	4950 3000 4950 3900
Wire Wire Line
	5350 2000 5350 2100
Wire Wire Line
	5350 3000 5350 2900
$Comp
L Switch:SW_Push SW9
U 1 1 5F447C77
P 5350 3300
F 0 "SW9" V 5350 3250 50  0000 R CNN
F 1 "Cherry MX" V 5300 3200 50  0001 R CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX_1.00u_PCB" H 5350 3500 50  0001 C CNN
F 3 "~" H 5350 3500 50  0001 C CNN
	1    5350 3300
	0    -1   -1   0   
$EndComp
Wire Wire Line
	5350 3900 5350 3800
Text Notes 5100 3350 0    50   ~ 0
5
$Comp
L Diode:1N4148 D8
U 1 1 5F447C9D
P 5350 3650
F 0 "D8" V 5450 3550 50  0000 C CNN
F 1 "1N4148" V 5450 3850 50  0000 C CNN
F 2 "Diode_THT:D_DO-35_SOD27_P7.62mm_Horizontal" H 5350 3475 50  0001 C CNN
F 3 "https://assets.nexperia.com/documents/data-sheet/1N4148_1N4448.pdf" H 5350 3650 50  0001 C CNN
	1    5350 3650
	0    -1   -1   0   
$EndComp
$Comp
L Switch:SW_Push SW7
U 1 1 5F447CA3
P 5350 1500
F 0 "SW7" V 5350 1450 50  0000 R CNN
F 1 "Cherry MX" V 5300 1400 50  0001 R CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX_1.00u_PCB" H 5350 1700 50  0001 C CNN
F 3 "~" H 5350 1700 50  0001 C CNN
	1    5350 1500
	0    -1   -1   0   
$EndComp
$Comp
L Diode:1N4148 D6
U 1 1 5F447CA9
P 5350 1850
F 0 "D6" V 5450 1750 50  0000 C CNN
F 1 "1N4148" V 5450 2050 50  0000 C CNN
F 2 "Diode_THT:D_DO-35_SOD27_P7.62mm_Horizontal" H 5350 1675 50  0001 C CNN
F 3 "https://assets.nexperia.com/documents/data-sheet/1N4148_1N4448.pdf" H 5350 1850 50  0001 C CNN
	1    5350 1850
	0    -1   -1   0   
$EndComp
$Comp
L Diode:1N4148 D7
U 1 1 5F447CAF
P 5350 2750
F 0 "D7" V 5450 2650 50  0000 C CNN
F 1 "1N4148" V 5450 2950 50  0000 C CNN
F 2 "Diode_THT:D_DO-35_SOD27_P7.62mm_Horizontal" H 5350 2575 50  0001 C CNN
F 3 "https://assets.nexperia.com/documents/data-sheet/1N4148_1N4448.pdf" H 5350 2750 50  0001 C CNN
	1    5350 2750
	0    -1   -1   0   
$EndComp
$Comp
L Switch:SW_Push SW8
U 1 1 5F447CB5
P 5350 2400
F 0 "SW8" V 5350 2350 50  0000 R CNN
F 1 "Cherry MX" V 5300 2300 50  0001 R CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX_1.00u_PCB" H 5350 2600 50  0001 C CNN
F 3 "~" H 5350 2600 50  0001 C CNN
	1    5350 2400
	0    -1   -1   0   
$EndComp
Wire Wire Line
	5350 2100 5650 2100
Wire Wire Line
	5650 2100 5650 3000
Wire Wire Line
	5350 3900 5650 3900
Wire Wire Line
	5350 3000 5650 3000
Connection ~ 5650 3000
Wire Wire Line
	5650 3000 5650 3900
Wire Wire Line
	4650 3100 5350 3100
Wire Wire Line
	4650 2200 5350 2200
Wire Wire Line
	4650 1300 5350 1300
Text Notes 5100 2450 0    50   ~ 0
8
Text Notes 5100 1550 0    50   ~ 0
/
Wire Wire Line
	6050 2000 6050 2100
Wire Wire Line
	6050 3000 6050 2900
$Comp
L Switch:SW_Push SW13
U 1 1 5F4794A7
P 6050 3300
F 0 "SW13" V 6050 3250 50  0000 R CNN
F 1 "Cherry MX" V 6000 3200 50  0001 R CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX_1.00u_PCB" H 6050 3500 50  0001 C CNN
F 3 "~" H 6050 3500 50  0001 C CNN
	1    6050 3300
	0    -1   -1   0   
$EndComp
Wire Wire Line
	6050 3900 6050 3800
Text Notes 5800 3350 0    50   ~ 0
6
$Comp
L Diode:1N4148 D12
U 1 1 5F4794CA
P 6050 3650
F 0 "D12" V 6150 3550 50  0000 C CNN
F 1 "1N4148" V 6150 3850 50  0000 C CNN
F 2 "Diode_THT:D_DO-35_SOD27_P7.62mm_Horizontal" H 6050 3475 50  0001 C CNN
F 3 "https://assets.nexperia.com/documents/data-sheet/1N4148_1N4448.pdf" H 6050 3650 50  0001 C CNN
	1    6050 3650
	0    -1   -1   0   
$EndComp
$Comp
L Switch:SW_Push SW11
U 1 1 5F4794D0
P 6050 1500
F 0 "SW11" V 6050 1450 50  0000 R CNN
F 1 "Cherry MX" V 6000 1400 50  0001 R CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX_1.00u_PCB" H 6050 1700 50  0001 C CNN
F 3 "~" H 6050 1700 50  0001 C CNN
	1    6050 1500
	0    -1   -1   0   
$EndComp
$Comp
L Diode:1N4148 D10
U 1 1 5F4794D6
P 6050 1850
F 0 "D10" V 6150 1750 50  0000 C CNN
F 1 "1N4148" V 6150 2050 50  0000 C CNN
F 2 "Diode_THT:D_DO-35_SOD27_P7.62mm_Horizontal" H 6050 1675 50  0001 C CNN
F 3 "https://assets.nexperia.com/documents/data-sheet/1N4148_1N4448.pdf" H 6050 1850 50  0001 C CNN
	1    6050 1850
	0    -1   -1   0   
$EndComp
$Comp
L Diode:1N4148 D11
U 1 1 5F4794DC
P 6050 2750
F 0 "D11" V 6150 2650 50  0000 C CNN
F 1 "1N4148" V 6150 2950 50  0000 C CNN
F 2 "Diode_THT:D_DO-35_SOD27_P7.62mm_Horizontal" H 6050 2575 50  0001 C CNN
F 3 "https://assets.nexperia.com/documents/data-sheet/1N4148_1N4448.pdf" H 6050 2750 50  0001 C CNN
	1    6050 2750
	0    -1   -1   0   
$EndComp
$Comp
L Switch:SW_Push SW12
U 1 1 5F4794E2
P 6050 2400
F 0 "SW12" V 6050 2350 50  0000 R CNN
F 1 "Cherry MX" V 6000 2300 50  0001 R CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX_1.00u_PCB" H 6050 2600 50  0001 C CNN
F 3 "~" H 6050 2600 50  0001 C CNN
	1    6050 2400
	0    -1   -1   0   
$EndComp
Wire Wire Line
	6050 2100 6350 2100
Wire Wire Line
	6350 2100 6350 3000
Wire Wire Line
	6050 3900 6350 3900
Wire Wire Line
	6050 3000 6350 3000
Connection ~ 6350 3000
Wire Wire Line
	6350 3000 6350 3900
Wire Wire Line
	5350 3100 6050 3100
Wire Wire Line
	5350 2200 6050 2200
Wire Wire Line
	5350 1300 6050 1300
Text Notes 5800 2450 0    50   ~ 0
9
Text Notes 5800 1550 0    50   ~ 0
*
Connection ~ 5350 1300
Connection ~ 5350 2200
Connection ~ 5350 3100
Wire Wire Line
	4650 1300 4450 1300
Connection ~ 4650 1300
Wire Wire Line
	4450 2200 4650 2200
Connection ~ 4650 2200
Wire Wire Line
	4450 3100 4650 3100
Connection ~ 4650 3100
Text HLabel 7050 5900 3    50   Input ~ 0
COL03
Wire Wire Line
	4950 5700 4950 5900
Wire Wire Line
	6350 5700 6350 5900
Text HLabel 4950 5900 3    50   Input ~ 0
COL00
Text HLabel 5650 5900 3    50   Input ~ 0
COL01
Text HLabel 6350 5900 3    50   Input ~ 0
COL02
Wire Wire Line
	7050 5900 7050 5700
Wire Wire Line
	7050 2100 7050 3900
$Comp
L Diode:1N4148 D17
U 1 1 5F391D86
P 6750 5450
F 0 "D17" V 6850 5350 50  0000 C CNN
F 1 "1N4148" V 6850 5650 50  0000 C CNN
F 2 "Diode_THT:D_DO-35_SOD27_P7.62mm_Horizontal" H 6750 5275 50  0001 C CNN
F 3 "https://assets.nexperia.com/documents/data-sheet/1N4148_1N4448.pdf" H 6750 5450 50  0001 C CNN
	1    6750 5450
	0    -1   -1   0   
$EndComp
$Comp
L Switch:SW_Push SW18
U 1 1 5F391D8C
P 6750 5100
F 0 "SW18" V 6750 5050 50  0000 R CNN
F 1 "Cherry MX" V 6700 5000 50  0001 R CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX_1.00u_PCB" H 6750 5300 50  0001 C CNN
F 3 "~" H 6750 5300 50  0001 C CNN
	1    6750 5100
	0    -1   -1   0   
$EndComp
Wire Wire Line
	6050 4900 6750 4900
Wire Wire Line
	6750 5700 6750 5600
Text Notes 6400 5150 0    50   ~ 0
Enter
Wire Wire Line
	6750 5700 7050 5700
Connection ~ 6050 4900
Wire Wire Line
	6350 4800 6350 5700
Wire Wire Line
	6050 5700 6350 5700
Text Notes 5800 5150 0    50   ~ 0
.
Wire Wire Line
	6050 5700 6050 5600
$Comp
L Switch:SW_Push SW15
U 1 1 5F391D9C
P 6050 5100
F 0 "SW15" V 6050 5050 50  0000 R CNN
F 1 "Cherry MX" V 6000 5000 50  0001 R CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX_1.00u_PCB" H 6050 5300 50  0001 C CNN
F 3 "~" H 6050 5300 50  0001 C CNN
	1    6050 5100
	0    -1   -1   0   
$EndComp
$Comp
L Diode:1N4148 D14
U 1 1 5F391DA2
P 6050 5450
F 0 "D14" V 6150 5350 50  0000 C CNN
F 1 "1N4148" V 6150 5650 50  0000 C CNN
F 2 "Diode_THT:D_DO-35_SOD27_P7.62mm_Horizontal" H 6050 5275 50  0001 C CNN
F 3 "https://assets.nexperia.com/documents/data-sheet/1N4148_1N4448.pdf" H 6050 5450 50  0001 C CNN
	1    6050 5450
	0    -1   -1   0   
$EndComp
Wire Wire Line
	4950 4800 4950 5700
Wire Wire Line
	4650 5700 4950 5700
Text Notes 4400 5150 0    50   ~ 0
0
Wire Wire Line
	4650 5700 4650 5600
Wire Wire Line
	4450 4900 4650 4900
Connection ~ 4650 4900
$Comp
L Switch:SW_Push SW6
U 1 1 5F391DC1
P 4650 5100
F 0 "SW6" V 4650 5050 50  0000 R CNN
F 1 "Cherry MX" V 4600 5000 50  0001 R CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX_2.00u_PCB" H 4650 5300 50  0001 C CNN
F 3 "~" H 4650 5300 50  0001 C CNN
	1    4650 5100
	0    -1   -1   0   
$EndComp
$Comp
L Diode:1N4148 D5
U 1 1 5F391DC7
P 4650 5450
F 0 "D5" V 4750 5350 50  0000 C CNN
F 1 "1N4148" V 4750 5650 50  0000 C CNN
F 2 "Diode_THT:D_DO-35_SOD27_P7.62mm_Horizontal" H 4650 5275 50  0001 C CNN
F 3 "https://assets.nexperia.com/documents/data-sheet/1N4148_1N4448.pdf" H 4650 5450 50  0001 C CNN
	1    4650 5450
	0    -1   -1   0   
$EndComp
Text HLabel 4450 4900 0    50   Output ~ 0
ROW04
Connection ~ 4950 5700
Connection ~ 6350 5700
Connection ~ 7050 5700
Wire Wire Line
	4650 4900 6050 4900
Wire Wire Line
	7050 3900 7050 5700
Wire Wire Line
	5650 4800 5650 5900
$EndSCHEMATC
