EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 2 2
Title "QAZ Keys"
Date ""
Rev "v0.2"
Comp "Anthony Needles"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text HLabel 850  1200 0    50   Output ~ 0
ROW00
Text HLabel 850  2100 0    50   Output ~ 0
ROW01
Text HLabel 850  3000 0    50   Output ~ 0
ROW02
Text HLabel 850  3900 0    50   Output ~ 0
ROW03
Text HLabel 850  4800 0    50   Output ~ 0
ROW04
Text HLabel 1350 5800 3    50   Input ~ 0
COL00
Text HLabel 2050 5800 3    50   Input ~ 0
COL01
Text HLabel 2750 5800 3    50   Input ~ 0
COL02
Text HLabel 3450 5800 3    50   Input ~ 0
COL03
Text HLabel 4150 5800 3    50   Input ~ 0
COL04
Text HLabel 4850 5800 3    50   Input ~ 0
COL05
Text HLabel 5550 5800 3    50   Input ~ 0
COL06
Text HLabel 6250 5800 3    50   Input ~ 0
COL07
Text HLabel 6950 5800 3    50   Input ~ 0
COL08
Text HLabel 7650 5800 3    50   Input ~ 0
COL09
Text HLabel 8350 5800 3    50   Input ~ 0
COL10
Text HLabel 9050 5800 3    50   Input ~ 0
COL11
Text HLabel 9750 5800 3    50   Input ~ 0
COL12
Text HLabel 10450 5800 3    50   Input ~ 0
COL13
Text HLabel 11150 5800 3    50   Input ~ 0
COL14
Wire Wire Line
	1050 1900 1050 2000
Wire Wire Line
	1050 2900 1050 2800
$Comp
L Switch:SW_Push SW4
U 1 1 5F315CC3
P 1050 3200
F 0 "SW4" V 1050 3150 50  0000 R CNN
F 1 "Cherry MX" V 1000 3100 50  0001 R CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX_1.75u_PCB" H 1050 3400 50  0001 C CNN
F 3 "~" H 1050 3400 50  0001 C CNN
	1    1050 3200
	0    -1   -1   0   
$EndComp
Wire Wire Line
	1050 3800 1050 3700
Text Notes 700  3250 0    50   ~ 0
Caps\nLock
$Comp
L Diode:1N4148 D4
U 1 1 5F3167C6
P 1050 4450
F 0 "D4" V 1150 4350 50  0000 C CNN
F 1 "1N4148" V 1150 4650 50  0000 C CNN
F 2 "Diode_THT:D_DO-35_SOD27_P7.62mm_Horizontal" H 1050 4275 50  0001 C CNN
F 3 "https://assets.nexperia.com/documents/data-sheet/1N4148_1N4448.pdf" H 1050 4450 50  0001 C CNN
	1    1050 4450
	0    -1   -1   0   
$EndComp
$Comp
L Switch:SW_Push SW5
U 1 1 5F3167CC
P 1050 4100
F 0 "SW5" V 1050 4050 50  0000 R CNN
F 1 "Cherry MX" V 1000 4000 50  0001 R CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX_1.00u_PCB" H 1050 4300 50  0001 C CNN
F 3 "~" H 1050 4300 50  0001 C CNN
	1    1050 4100
	0    -1   -1   0   
$EndComp
Wire Wire Line
	1050 4700 1050 4600
Text Notes 700  4150 0    50   ~ 0
Left\nShift
$Comp
L Diode:1N4148 D5
U 1 1 5F3174AC
P 1050 5350
F 0 "D5" V 1150 5250 50  0000 C CNN
F 1 "1N4148" V 1150 5550 50  0000 C CNN
F 2 "Diode_THT:D_DO-35_SOD27_P7.62mm_Horizontal" H 1050 5175 50  0001 C CNN
F 3 "https://assets.nexperia.com/documents/data-sheet/1N4148_1N4448.pdf" H 1050 5350 50  0001 C CNN
	1    1050 5350
	0    -1   -1   0   
$EndComp
$Comp
L Switch:SW_Push SW6
U 1 1 5F3174B2
P 1050 5000
F 0 "SW6" V 1050 4950 50  0000 R CNN
F 1 "Cherry MX" V 1000 4900 50  0001 R CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX_1.25u_PCB" H 1050 5200 50  0001 C CNN
F 3 "~" H 1050 5200 50  0001 C CNN
	1    1050 5000
	0    -1   -1   0   
$EndComp
Wire Wire Line
	1050 5600 1050 5500
$Comp
L Diode:1N4148 D3
U 1 1 5F315CBD
P 1050 3550
F 0 "D3" V 1150 3450 50  0000 C CNN
F 1 "1N4148" V 1150 3750 50  0000 C CNN
F 2 "Diode_THT:D_DO-35_SOD27_P7.62mm_Horizontal" H 1050 3375 50  0001 C CNN
F 3 "https://assets.nexperia.com/documents/data-sheet/1N4148_1N4448.pdf" H 1050 3550 50  0001 C CNN
	1    1050 3550
	0    -1   -1   0   
$EndComp
Text Notes 700  1450 0    50   ~ 0
Esc
$Comp
L Switch:SW_Push SW2
U 1 1 5F30B6DF
P 1050 1400
F 0 "SW2" V 1050 1350 50  0000 R CNN
F 1 "Cherry MX" V 1000 1300 50  0001 R CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX_1.00u_PCB" H 1050 1600 50  0001 C CNN
F 3 "~" H 1050 1600 50  0001 C CNN
	1    1050 1400
	0    -1   -1   0   
$EndComp
Text Notes 700  2350 0    50   ~ 0
Tab
$Comp
L Diode:1N4148 D1
U 1 1 5F2DC5D8
P 1050 1750
F 0 "D1" V 1150 1650 50  0000 C CNN
F 1 "1N4148" V 1150 1950 50  0000 C CNN
F 2 "Diode_THT:D_DO-35_SOD27_P7.62mm_Horizontal" H 1050 1575 50  0001 C CNN
F 3 "https://assets.nexperia.com/documents/data-sheet/1N4148_1N4448.pdf" H 1050 1750 50  0001 C CNN
	1    1050 1750
	0    -1   -1   0   
$EndComp
$Comp
L Diode:1N4148 D2
U 1 1 5F3117C8
P 1050 2650
F 0 "D2" V 1150 2550 50  0000 C CNN
F 1 "1N4148" V 1150 2850 50  0000 C CNN
F 2 "Diode_THT:D_DO-35_SOD27_P7.62mm_Horizontal" H 1050 2475 50  0001 C CNN
F 3 "https://assets.nexperia.com/documents/data-sheet/1N4148_1N4448.pdf" H 1050 2650 50  0001 C CNN
	1    1050 2650
	0    -1   -1   0   
$EndComp
$Comp
L Switch:SW_Push SW3
U 1 1 5F3117CE
P 1050 2300
F 0 "SW3" V 1050 2250 50  0000 R CNN
F 1 "Cherry MX" V 1000 2200 50  0001 R CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX_1.50u_PCB" H 1050 2500 50  0001 C CNN
F 3 "~" H 1050 2500 50  0001 C CNN
	1    1050 2300
	0    -1   -1   0   
$EndComp
Wire Wire Line
	1050 2000 1350 2000
Wire Wire Line
	1350 5600 1050 5600
Wire Wire Line
	1350 2000 1350 2900
Wire Wire Line
	1050 4700 1350 4700
Connection ~ 1350 4700
Wire Wire Line
	1350 4700 1350 5600
Wire Wire Line
	1050 3800 1350 3800
Connection ~ 1350 3800
Wire Wire Line
	1350 3800 1350 4700
Wire Wire Line
	1050 2900 1350 2900
Connection ~ 1350 2900
Wire Wire Line
	1350 2900 1350 3800
Wire Wire Line
	1750 1900 1750 2000
Wire Wire Line
	1750 2900 1750 2800
$Comp
L Switch:SW_Push SW9
U 1 1 5F447C77
P 1750 3200
F 0 "SW9" V 1750 3150 50  0000 R CNN
F 1 "Cherry MX" V 1700 3100 50  0001 R CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX_1.00u_PCB" H 1750 3400 50  0001 C CNN
F 3 "~" H 1750 3400 50  0001 C CNN
	1    1750 3200
	0    -1   -1   0   
$EndComp
Wire Wire Line
	1750 3800 1750 3700
Text Notes 1450 3250 0    50   ~ 0
aA
$Comp
L Diode:1N4148 D9
U 1 1 5F447C80
P 1750 4450
F 0 "D9" V 1850 4350 50  0000 C CNN
F 1 "1N4148" V 1850 4650 50  0000 C CNN
F 2 "Diode_THT:D_DO-35_SOD27_P7.62mm_Horizontal" H 1750 4275 50  0001 C CNN
F 3 "https://assets.nexperia.com/documents/data-sheet/1N4148_1N4448.pdf" H 1750 4450 50  0001 C CNN
	1    1750 4450
	0    -1   -1   0   
$EndComp
$Comp
L Switch:SW_Push SW10
U 1 1 5F447C86
P 1750 4100
F 0 "SW10" V 1750 4050 50  0000 R CNN
F 1 "Cherry MX" V 1700 4000 50  0001 R CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX_1.00u_PCB" H 1750 4300 50  0001 C CNN
F 3 "~" H 1750 4300 50  0001 C CNN
	1    1750 4100
	0    -1   -1   0   
$EndComp
Wire Wire Line
	1750 4700 1750 4600
Text Notes 1450 4150 0    50   ~ 0
zZ
$Comp
L Diode:1N4148 D10
U 1 1 5F447C8F
P 1750 5350
F 0 "D10" V 1850 5250 50  0000 C CNN
F 1 "1N4148" V 1850 5550 50  0000 C CNN
F 2 "Diode_THT:D_DO-35_SOD27_P7.62mm_Horizontal" H 1750 5175 50  0001 C CNN
F 3 "https://assets.nexperia.com/documents/data-sheet/1N4148_1N4448.pdf" H 1750 5350 50  0001 C CNN
	1    1750 5350
	0    -1   -1   0   
$EndComp
$Comp
L Switch:SW_Push SW11
U 1 1 5F447C95
P 1750 5000
F 0 "SW11" V 1750 4950 50  0000 R CNN
F 1 "Cherry MX" V 1700 4900 50  0001 R CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX_1.25u_PCB" H 1750 5200 50  0001 C CNN
F 3 "~" H 1750 5200 50  0001 C CNN
	1    1750 5000
	0    -1   -1   0   
$EndComp
Wire Wire Line
	1750 5600 1750 5500
$Comp
L Diode:1N4148 D8
U 1 1 5F447C9D
P 1750 3550
F 0 "D8" V 1850 3450 50  0000 C CNN
F 1 "1N4148" V 1850 3750 50  0000 C CNN
F 2 "Diode_THT:D_DO-35_SOD27_P7.62mm_Horizontal" H 1750 3375 50  0001 C CNN
F 3 "https://assets.nexperia.com/documents/data-sheet/1N4148_1N4448.pdf" H 1750 3550 50  0001 C CNN
	1    1750 3550
	0    -1   -1   0   
$EndComp
$Comp
L Switch:SW_Push SW7
U 1 1 5F447CA3
P 1750 1400
F 0 "SW7" V 1750 1350 50  0000 R CNN
F 1 "Cherry MX" V 1700 1300 50  0001 R CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX_1.00u_PCB" H 1750 1600 50  0001 C CNN
F 3 "~" H 1750 1600 50  0001 C CNN
	1    1750 1400
	0    -1   -1   0   
$EndComp
$Comp
L Diode:1N4148 D6
U 1 1 5F447CA9
P 1750 1750
F 0 "D6" V 1850 1650 50  0000 C CNN
F 1 "1N4148" V 1850 1950 50  0000 C CNN
F 2 "Diode_THT:D_DO-35_SOD27_P7.62mm_Horizontal" H 1750 1575 50  0001 C CNN
F 3 "https://assets.nexperia.com/documents/data-sheet/1N4148_1N4448.pdf" H 1750 1750 50  0001 C CNN
	1    1750 1750
	0    -1   -1   0   
$EndComp
$Comp
L Diode:1N4148 D7
U 1 1 5F447CAF
P 1750 2650
F 0 "D7" V 1850 2550 50  0000 C CNN
F 1 "1N4148" V 1850 2850 50  0000 C CNN
F 2 "Diode_THT:D_DO-35_SOD27_P7.62mm_Horizontal" H 1750 2475 50  0001 C CNN
F 3 "https://assets.nexperia.com/documents/data-sheet/1N4148_1N4448.pdf" H 1750 2650 50  0001 C CNN
	1    1750 2650
	0    -1   -1   0   
$EndComp
$Comp
L Switch:SW_Push SW8
U 1 1 5F447CB5
P 1750 2300
F 0 "SW8" V 1750 2250 50  0000 R CNN
F 1 "Cherry MX" V 1700 2200 50  0001 R CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX_1.00u_PCB" H 1750 2500 50  0001 C CNN
F 3 "~" H 1750 2500 50  0001 C CNN
	1    1750 2300
	0    -1   -1   0   
$EndComp
Wire Wire Line
	1750 2000 2050 2000
Wire Wire Line
	2050 5600 1750 5600
Wire Wire Line
	2050 2000 2050 2900
Wire Wire Line
	1750 4700 2050 4700
Connection ~ 2050 4700
Wire Wire Line
	2050 4700 2050 5600
Wire Wire Line
	1750 3800 2050 3800
Connection ~ 2050 3800
Wire Wire Line
	2050 3800 2050 4700
Wire Wire Line
	1750 2900 2050 2900
Connection ~ 2050 2900
Wire Wire Line
	2050 2900 2050 3800
Wire Wire Line
	1050 4800 1750 4800
Wire Wire Line
	1050 3900 1750 3900
Wire Wire Line
	1050 3000 1750 3000
Wire Wire Line
	1050 2100 1750 2100
Wire Wire Line
	1050 1200 1750 1200
Text Notes 1450 2350 0    50   ~ 0
qQ
Text Notes 1450 1450 0    50   ~ 0
1!
Text Notes 700  5050 0    50   ~ 0
Left\nCtrl
Text Notes 1450 5050 0    50   ~ 0
Left\nGUI
Wire Wire Line
	2450 1900 2450 2000
Wire Wire Line
	2450 2900 2450 2800
$Comp
L Switch:SW_Push SW14
U 1 1 5F4794A7
P 2450 3200
F 0 "SW14" V 2450 3150 50  0000 R CNN
F 1 "Cherry MX" V 2400 3100 50  0001 R CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX_1.00u_PCB" H 2450 3400 50  0001 C CNN
F 3 "~" H 2450 3400 50  0001 C CNN
	1    2450 3200
	0    -1   -1   0   
$EndComp
Wire Wire Line
	2450 3800 2450 3700
Text Notes 2150 3250 0    50   ~ 0
sS
$Comp
L Diode:1N4148 D14
U 1 1 5F4794AF
P 2450 4450
F 0 "D14" V 2550 4350 50  0000 C CNN
F 1 "1N4148" V 2550 4650 50  0000 C CNN
F 2 "Diode_THT:D_DO-35_SOD27_P7.62mm_Horizontal" H 2450 4275 50  0001 C CNN
F 3 "https://assets.nexperia.com/documents/data-sheet/1N4148_1N4448.pdf" H 2450 4450 50  0001 C CNN
	1    2450 4450
	0    -1   -1   0   
$EndComp
$Comp
L Switch:SW_Push SW15
U 1 1 5F4794B5
P 2450 4100
F 0 "SW15" V 2450 4050 50  0000 R CNN
F 1 "Cherry MX" V 2400 4000 50  0001 R CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX_1.00u_PCB" H 2450 4300 50  0001 C CNN
F 3 "~" H 2450 4300 50  0001 C CNN
	1    2450 4100
	0    -1   -1   0   
$EndComp
Wire Wire Line
	2450 4700 2450 4600
Text Notes 2150 4150 0    50   ~ 0
xX
$Comp
L Diode:1N4148 D15
U 1 1 5F4794BD
P 2450 5350
F 0 "D15" V 2550 5250 50  0000 C CNN
F 1 "1N4148" V 2550 5550 50  0000 C CNN
F 2 "Diode_THT:D_DO-35_SOD27_P7.62mm_Horizontal" H 2450 5175 50  0001 C CNN
F 3 "https://assets.nexperia.com/documents/data-sheet/1N4148_1N4448.pdf" H 2450 5350 50  0001 C CNN
	1    2450 5350
	0    -1   -1   0   
$EndComp
$Comp
L Switch:SW_Push SW16
U 1 1 5F4794C3
P 2450 5000
F 0 "SW16" V 2450 4950 50  0000 R CNN
F 1 "Cherry MX" V 2400 4900 50  0001 R CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX_1.25u_PCB" H 2450 5200 50  0001 C CNN
F 3 "~" H 2450 5200 50  0001 C CNN
	1    2450 5000
	0    -1   -1   0   
$EndComp
Wire Wire Line
	2450 5600 2450 5500
$Comp
L Diode:1N4148 D13
U 1 1 5F4794CA
P 2450 3550
F 0 "D13" V 2550 3450 50  0000 C CNN
F 1 "1N4148" V 2550 3750 50  0000 C CNN
F 2 "Diode_THT:D_DO-35_SOD27_P7.62mm_Horizontal" H 2450 3375 50  0001 C CNN
F 3 "https://assets.nexperia.com/documents/data-sheet/1N4148_1N4448.pdf" H 2450 3550 50  0001 C CNN
	1    2450 3550
	0    -1   -1   0   
$EndComp
$Comp
L Switch:SW_Push SW12
U 1 1 5F4794D0
P 2450 1400
F 0 "SW12" V 2450 1350 50  0000 R CNN
F 1 "Cherry MX" V 2400 1300 50  0001 R CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX_1.00u_PCB" H 2450 1600 50  0001 C CNN
F 3 "~" H 2450 1600 50  0001 C CNN
	1    2450 1400
	0    -1   -1   0   
$EndComp
$Comp
L Diode:1N4148 D11
U 1 1 5F4794D6
P 2450 1750
F 0 "D11" V 2550 1650 50  0000 C CNN
F 1 "1N4148" V 2550 1950 50  0000 C CNN
F 2 "Diode_THT:D_DO-35_SOD27_P7.62mm_Horizontal" H 2450 1575 50  0001 C CNN
F 3 "https://assets.nexperia.com/documents/data-sheet/1N4148_1N4448.pdf" H 2450 1750 50  0001 C CNN
	1    2450 1750
	0    -1   -1   0   
$EndComp
$Comp
L Diode:1N4148 D12
U 1 1 5F4794DC
P 2450 2650
F 0 "D12" V 2550 2550 50  0000 C CNN
F 1 "1N4148" V 2550 2850 50  0000 C CNN
F 2 "Diode_THT:D_DO-35_SOD27_P7.62mm_Horizontal" H 2450 2475 50  0001 C CNN
F 3 "https://assets.nexperia.com/documents/data-sheet/1N4148_1N4448.pdf" H 2450 2650 50  0001 C CNN
	1    2450 2650
	0    -1   -1   0   
$EndComp
$Comp
L Switch:SW_Push SW13
U 1 1 5F4794E2
P 2450 2300
F 0 "SW13" V 2450 2250 50  0000 R CNN
F 1 "Cherry MX" V 2400 2200 50  0001 R CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX_1.00u_PCB" H 2450 2500 50  0001 C CNN
F 3 "~" H 2450 2500 50  0001 C CNN
	1    2450 2300
	0    -1   -1   0   
$EndComp
Wire Wire Line
	2450 2000 2750 2000
Wire Wire Line
	2750 5600 2450 5600
Wire Wire Line
	2750 2000 2750 2900
Wire Wire Line
	2450 4700 2750 4700
Connection ~ 2750 4700
Wire Wire Line
	2750 4700 2750 5600
Wire Wire Line
	2450 3800 2750 3800
Connection ~ 2750 3800
Wire Wire Line
	2750 3800 2750 4700
Wire Wire Line
	2450 2900 2750 2900
Connection ~ 2750 2900
Wire Wire Line
	2750 2900 2750 3800
Wire Wire Line
	1750 4800 2450 4800
Wire Wire Line
	1750 3900 2450 3900
Wire Wire Line
	1750 3000 2450 3000
Wire Wire Line
	1750 2100 2450 2100
Wire Wire Line
	1750 1200 2450 1200
Text Notes 2150 2350 0    50   ~ 0
wW
Text Notes 2150 1450 0    50   ~ 0
2@
Text Notes 2150 5050 0    50   ~ 0
Left\nAlt
Wire Wire Line
	3150 1900 3150 2000
Wire Wire Line
	3150 2900 3150 2800
$Comp
L Switch:SW_Push SW19
U 1 1 5F48713E
P 3150 3200
F 0 "SW19" V 3150 3150 50  0000 R CNN
F 1 "Cherry MX" V 3100 3100 50  0001 R CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX_1.00u_PCB" H 3150 3400 50  0001 C CNN
F 3 "~" H 3150 3400 50  0001 C CNN
	1    3150 3200
	0    -1   -1   0   
$EndComp
Wire Wire Line
	3150 3800 3150 3700
Text Notes 2850 3250 0    50   ~ 0
dD
$Comp
L Diode:1N4148 D19
U 1 1 5F487146
P 3150 4450
F 0 "D19" V 3250 4350 50  0000 C CNN
F 1 "1N4148" V 3250 4650 50  0000 C CNN
F 2 "Diode_THT:D_DO-35_SOD27_P7.62mm_Horizontal" H 3150 4275 50  0001 C CNN
F 3 "https://assets.nexperia.com/documents/data-sheet/1N4148_1N4448.pdf" H 3150 4450 50  0001 C CNN
	1    3150 4450
	0    -1   -1   0   
$EndComp
$Comp
L Switch:SW_Push SW20
U 1 1 5F48714C
P 3150 4100
F 0 "SW20" V 3150 4050 50  0000 R CNN
F 1 "Cherry MX" V 3100 4000 50  0001 R CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX_1.00u_PCB" H 3150 4300 50  0001 C CNN
F 3 "~" H 3150 4300 50  0001 C CNN
	1    3150 4100
	0    -1   -1   0   
$EndComp
Wire Wire Line
	3150 4700 3150 4600
Text Notes 2850 4150 0    50   ~ 0
cC
$Comp
L Diode:1N4148 D18
U 1 1 5F487161
P 3150 3550
F 0 "D18" V 3250 3450 50  0000 C CNN
F 1 "1N4148" V 3250 3750 50  0000 C CNN
F 2 "Diode_THT:D_DO-35_SOD27_P7.62mm_Horizontal" H 3150 3375 50  0001 C CNN
F 3 "https://assets.nexperia.com/documents/data-sheet/1N4148_1N4448.pdf" H 3150 3550 50  0001 C CNN
	1    3150 3550
	0    -1   -1   0   
$EndComp
$Comp
L Switch:SW_Push SW17
U 1 1 5F487167
P 3150 1400
F 0 "SW17" V 3150 1350 50  0000 R CNN
F 1 "Cherry MX" V 3100 1300 50  0001 R CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX_1.00u_PCB" H 3150 1600 50  0001 C CNN
F 3 "~" H 3150 1600 50  0001 C CNN
	1    3150 1400
	0    -1   -1   0   
$EndComp
$Comp
L Diode:1N4148 D16
U 1 1 5F48716D
P 3150 1750
F 0 "D16" V 3250 1650 50  0000 C CNN
F 1 "1N4148" V 3250 1950 50  0000 C CNN
F 2 "Diode_THT:D_DO-35_SOD27_P7.62mm_Horizontal" H 3150 1575 50  0001 C CNN
F 3 "https://assets.nexperia.com/documents/data-sheet/1N4148_1N4448.pdf" H 3150 1750 50  0001 C CNN
	1    3150 1750
	0    -1   -1   0   
$EndComp
$Comp
L Diode:1N4148 D17
U 1 1 5F487173
P 3150 2650
F 0 "D17" V 3250 2550 50  0000 C CNN
F 1 "1N4148" V 3250 2850 50  0000 C CNN
F 2 "Diode_THT:D_DO-35_SOD27_P7.62mm_Horizontal" H 3150 2475 50  0001 C CNN
F 3 "https://assets.nexperia.com/documents/data-sheet/1N4148_1N4448.pdf" H 3150 2650 50  0001 C CNN
	1    3150 2650
	0    -1   -1   0   
$EndComp
$Comp
L Switch:SW_Push SW18
U 1 1 5F487179
P 3150 2300
F 0 "SW18" V 3150 2250 50  0000 R CNN
F 1 "Cherry MX" V 3100 2200 50  0001 R CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX_1.00u_PCB" H 3150 2500 50  0001 C CNN
F 3 "~" H 3150 2500 50  0001 C CNN
	1    3150 2300
	0    -1   -1   0   
$EndComp
Wire Wire Line
	3150 2000 3450 2000
Wire Wire Line
	3450 2000 3450 2900
Wire Wire Line
	3150 4700 3450 4700
Connection ~ 3450 4700
Wire Wire Line
	3150 3800 3450 3800
Connection ~ 3450 3800
Wire Wire Line
	3450 3800 3450 4700
Wire Wire Line
	3150 2900 3450 2900
Connection ~ 3450 2900
Wire Wire Line
	3450 2900 3450 3800
Wire Wire Line
	2450 3900 3150 3900
Wire Wire Line
	2450 3000 3150 3000
Wire Wire Line
	2450 2100 3150 2100
Wire Wire Line
	2450 1200 3150 1200
Text Notes 2850 2350 0    50   ~ 0
eE
Text Notes 2850 1450 0    50   ~ 0
3#
Wire Wire Line
	3850 1900 3850 2000
Wire Wire Line
	3850 2900 3850 2800
$Comp
L Switch:SW_Push SW23
U 1 1 5F487195
P 3850 3200
F 0 "SW23" V 3850 3150 50  0000 R CNN
F 1 "Cherry MX" V 3800 3100 50  0001 R CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX_1.00u_PCB" H 3850 3400 50  0001 C CNN
F 3 "~" H 3850 3400 50  0001 C CNN
	1    3850 3200
	0    -1   -1   0   
$EndComp
Wire Wire Line
	3850 3800 3850 3700
Text Notes 3550 3250 0    50   ~ 0
fF
$Comp
L Diode:1N4148 D23
U 1 1 5F48719D
P 3850 4450
F 0 "D23" V 3950 4350 50  0000 C CNN
F 1 "1N4148" V 3950 4650 50  0000 C CNN
F 2 "Diode_THT:D_DO-35_SOD27_P7.62mm_Horizontal" H 3850 4275 50  0001 C CNN
F 3 "https://assets.nexperia.com/documents/data-sheet/1N4148_1N4448.pdf" H 3850 4450 50  0001 C CNN
	1    3850 4450
	0    -1   -1   0   
$EndComp
$Comp
L Switch:SW_Push SW24
U 1 1 5F4871A3
P 3850 4100
F 0 "SW24" V 3850 4050 50  0000 R CNN
F 1 "Cherry MX" V 3800 4000 50  0001 R CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX_1.00u_PCB" H 3850 4300 50  0001 C CNN
F 3 "~" H 3850 4300 50  0001 C CNN
	1    3850 4100
	0    -1   -1   0   
$EndComp
Wire Wire Line
	3850 4700 3850 4600
Text Notes 3550 4150 0    50   ~ 0
vV
$Comp
L Diode:1N4148 D22
U 1 1 5F4871B8
P 3850 3550
F 0 "D22" V 3950 3450 50  0000 C CNN
F 1 "1N4148" V 3950 3750 50  0000 C CNN
F 2 "Diode_THT:D_DO-35_SOD27_P7.62mm_Horizontal" H 3850 3375 50  0001 C CNN
F 3 "https://assets.nexperia.com/documents/data-sheet/1N4148_1N4448.pdf" H 3850 3550 50  0001 C CNN
	1    3850 3550
	0    -1   -1   0   
$EndComp
$Comp
L Switch:SW_Push SW21
U 1 1 5F4871BE
P 3850 1400
F 0 "SW21" V 3850 1350 50  0000 R CNN
F 1 "Cherry MX" V 3800 1300 50  0001 R CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX_1.00u_PCB" H 3850 1600 50  0001 C CNN
F 3 "~" H 3850 1600 50  0001 C CNN
	1    3850 1400
	0    -1   -1   0   
$EndComp
$Comp
L Diode:1N4148 D20
U 1 1 5F4871C4
P 3850 1750
F 0 "D20" V 3950 1650 50  0000 C CNN
F 1 "1N4148" V 3950 1950 50  0000 C CNN
F 2 "Diode_THT:D_DO-35_SOD27_P7.62mm_Horizontal" H 3850 1575 50  0001 C CNN
F 3 "https://assets.nexperia.com/documents/data-sheet/1N4148_1N4448.pdf" H 3850 1750 50  0001 C CNN
	1    3850 1750
	0    -1   -1   0   
$EndComp
$Comp
L Diode:1N4148 D21
U 1 1 5F4871CA
P 3850 2650
F 0 "D21" V 3950 2550 50  0000 C CNN
F 1 "1N4148" V 3950 2850 50  0000 C CNN
F 2 "Diode_THT:D_DO-35_SOD27_P7.62mm_Horizontal" H 3850 2475 50  0001 C CNN
F 3 "https://assets.nexperia.com/documents/data-sheet/1N4148_1N4448.pdf" H 3850 2650 50  0001 C CNN
	1    3850 2650
	0    -1   -1   0   
$EndComp
$Comp
L Switch:SW_Push SW22
U 1 1 5F4871D0
P 3850 2300
F 0 "SW22" V 3850 2250 50  0000 R CNN
F 1 "Cherry MX" V 3800 2200 50  0001 R CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX_1.00u_PCB" H 3850 2500 50  0001 C CNN
F 3 "~" H 3850 2500 50  0001 C CNN
	1    3850 2300
	0    -1   -1   0   
$EndComp
Wire Wire Line
	3850 2000 4150 2000
Wire Wire Line
	4150 2000 4150 2900
Wire Wire Line
	3850 4700 4150 4700
Connection ~ 4150 4700
Wire Wire Line
	3850 3800 4150 3800
Connection ~ 4150 3800
Wire Wire Line
	4150 3800 4150 4700
Wire Wire Line
	3850 2900 4150 2900
Connection ~ 4150 2900
Wire Wire Line
	4150 2900 4150 3800
Wire Wire Line
	3150 3900 3850 3900
Wire Wire Line
	3150 3000 3850 3000
Wire Wire Line
	3150 2100 3850 2100
Wire Wire Line
	3150 1200 3850 1200
Text Notes 3550 2350 0    50   ~ 0
rR
Text Notes 3550 1450 0    50   ~ 0
4$
Wire Wire Line
	4550 1900 4550 2000
Wire Wire Line
	4550 2900 4550 2800
$Comp
L Switch:SW_Push SW27
U 1 1 5F4A2465
P 4550 3200
F 0 "SW27" V 4550 3150 50  0000 R CNN
F 1 "Cherry MX" V 4500 3100 50  0001 R CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX_1.00u_PCB" H 4550 3400 50  0001 C CNN
F 3 "~" H 4550 3400 50  0001 C CNN
	1    4550 3200
	0    -1   -1   0   
$EndComp
Wire Wire Line
	4550 3800 4550 3700
$Comp
L Diode:1N4148 D27
U 1 1 5F4A246C
P 4550 4450
F 0 "D27" V 4650 4350 50  0000 C CNN
F 1 "1N4148" V 4650 4650 50  0000 C CNN
F 2 "Diode_THT:D_DO-35_SOD27_P7.62mm_Horizontal" H 4550 4275 50  0001 C CNN
F 3 "https://assets.nexperia.com/documents/data-sheet/1N4148_1N4448.pdf" H 4550 4450 50  0001 C CNN
	1    4550 4450
	0    -1   -1   0   
$EndComp
$Comp
L Switch:SW_Push SW28
U 1 1 5F4A2472
P 4550 4100
F 0 "SW28" V 4550 4050 50  0000 R CNN
F 1 "Cherry MX" V 4500 4000 50  0001 R CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX_1.00u_PCB" H 4550 4300 50  0001 C CNN
F 3 "~" H 4550 4300 50  0001 C CNN
	1    4550 4100
	0    -1   -1   0   
$EndComp
Wire Wire Line
	4550 4700 4550 4600
$Comp
L Diode:1N4148 D28
U 1 1 5F4A2479
P 4550 5350
F 0 "D28" V 4650 5250 50  0000 C CNN
F 1 "1N4148" V 4650 5550 50  0000 C CNN
F 2 "Diode_THT:D_DO-35_SOD27_P7.62mm_Horizontal" H 4550 5175 50  0001 C CNN
F 3 "https://assets.nexperia.com/documents/data-sheet/1N4148_1N4448.pdf" H 4550 5350 50  0001 C CNN
	1    4550 5350
	0    -1   -1   0   
$EndComp
$Comp
L Switch:SW_Push SW29
U 1 1 5F4A247F
P 4550 5000
F 0 "SW29" V 4550 4950 50  0000 R CNN
F 1 "Cherry MX" V 4500 4900 50  0001 R CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX_6.25u_PCB" H 4550 5200 50  0001 C CNN
F 3 "~" H 4550 5200 50  0001 C CNN
	1    4550 5000
	0    -1   -1   0   
$EndComp
Wire Wire Line
	4550 5600 4550 5500
$Comp
L Diode:1N4148 D26
U 1 1 5F4A2486
P 4550 3550
F 0 "D26" V 4650 3450 50  0000 C CNN
F 1 "1N4148" V 4650 3750 50  0000 C CNN
F 2 "Diode_THT:D_DO-35_SOD27_P7.62mm_Horizontal" H 4550 3375 50  0001 C CNN
F 3 "https://assets.nexperia.com/documents/data-sheet/1N4148_1N4448.pdf" H 4550 3550 50  0001 C CNN
	1    4550 3550
	0    -1   -1   0   
$EndComp
$Comp
L Switch:SW_Push SW25
U 1 1 5F4A248C
P 4550 1400
F 0 "SW25" V 4550 1350 50  0000 R CNN
F 1 "Cherry MX" V 4500 1300 50  0001 R CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX_1.00u_PCB" H 4550 1600 50  0001 C CNN
F 3 "~" H 4550 1600 50  0001 C CNN
	1    4550 1400
	0    -1   -1   0   
$EndComp
$Comp
L Diode:1N4148 D24
U 1 1 5F4A2492
P 4550 1750
F 0 "D24" V 4650 1650 50  0000 C CNN
F 1 "1N4148" V 4650 1950 50  0000 C CNN
F 2 "Diode_THT:D_DO-35_SOD27_P7.62mm_Horizontal" H 4550 1575 50  0001 C CNN
F 3 "https://assets.nexperia.com/documents/data-sheet/1N4148_1N4448.pdf" H 4550 1750 50  0001 C CNN
	1    4550 1750
	0    -1   -1   0   
$EndComp
$Comp
L Diode:1N4148 D25
U 1 1 5F4A2498
P 4550 2650
F 0 "D25" V 4650 2550 50  0000 C CNN
F 1 "1N4148" V 4650 2850 50  0000 C CNN
F 2 "Diode_THT:D_DO-35_SOD27_P7.62mm_Horizontal" H 4550 2475 50  0001 C CNN
F 3 "https://assets.nexperia.com/documents/data-sheet/1N4148_1N4448.pdf" H 4550 2650 50  0001 C CNN
	1    4550 2650
	0    -1   -1   0   
$EndComp
$Comp
L Switch:SW_Push SW26
U 1 1 5F4A249E
P 4550 2300
F 0 "SW26" V 4550 2250 50  0000 R CNN
F 1 "Cherry MX" V 4500 2200 50  0001 R CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX_1.00u_PCB" H 4550 2500 50  0001 C CNN
F 3 "~" H 4550 2500 50  0001 C CNN
	1    4550 2300
	0    -1   -1   0   
$EndComp
Wire Wire Line
	4550 2000 4850 2000
Wire Wire Line
	4850 5600 4550 5600
Wire Wire Line
	4850 2000 4850 2900
Wire Wire Line
	4550 4700 4850 4700
Connection ~ 4850 4700
Wire Wire Line
	4850 4700 4850 5600
Wire Wire Line
	4550 3800 4850 3800
Connection ~ 4850 3800
Wire Wire Line
	4850 3800 4850 4700
Wire Wire Line
	4550 2900 4850 2900
Connection ~ 4850 2900
Wire Wire Line
	4850 2900 4850 3800
Wire Wire Line
	3850 3900 4550 3900
Wire Wire Line
	3850 3000 4550 3000
Wire Wire Line
	3850 2100 4550 2100
Wire Wire Line
	3850 1200 4550 1200
Text Notes 4250 2350 0    50   ~ 0
tT
Text Notes 4200 5000 0    50   ~ 0
Space
Wire Wire Line
	5250 1900 5250 2000
Wire Wire Line
	5250 2900 5250 2800
$Comp
L Switch:SW_Push SW32
U 1 1 5F4A24B9
P 5250 3200
F 0 "SW32" V 5250 3150 50  0000 R CNN
F 1 "Cherry MX" V 5200 3100 50  0001 R CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX_1.00u_PCB" H 5250 3400 50  0001 C CNN
F 3 "~" H 5250 3400 50  0001 C CNN
	1    5250 3200
	0    -1   -1   0   
$EndComp
Wire Wire Line
	5250 3800 5250 3700
Text Notes 4950 3250 0    50   ~ 0
hH
$Comp
L Diode:1N4148 D32
U 1 1 5F4A24C1
P 5250 4450
F 0 "D32" V 5350 4350 50  0000 C CNN
F 1 "1N4148" V 5350 4650 50  0000 C CNN
F 2 "Diode_THT:D_DO-35_SOD27_P7.62mm_Horizontal" H 5250 4275 50  0001 C CNN
F 3 "https://assets.nexperia.com/documents/data-sheet/1N4148_1N4448.pdf" H 5250 4450 50  0001 C CNN
	1    5250 4450
	0    -1   -1   0   
$EndComp
$Comp
L Switch:SW_Push SW33
U 1 1 5F4A24C7
P 5250 4100
F 0 "SW33" V 5250 4050 50  0000 R CNN
F 1 "Cherry MX" V 5200 4000 50  0001 R CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX_1.00u_PCB" H 5250 4300 50  0001 C CNN
F 3 "~" H 5250 4300 50  0001 C CNN
	1    5250 4100
	0    -1   -1   0   
$EndComp
Wire Wire Line
	5250 4700 5250 4600
Text Notes 4950 4150 0    50   ~ 0
nN
$Comp
L Diode:1N4148 D31
U 1 1 5F4A24DC
P 5250 3550
F 0 "D31" V 5350 3450 50  0000 C CNN
F 1 "1N4148" V 5350 3750 50  0000 C CNN
F 2 "Diode_THT:D_DO-35_SOD27_P7.62mm_Horizontal" H 5250 3375 50  0001 C CNN
F 3 "https://assets.nexperia.com/documents/data-sheet/1N4148_1N4448.pdf" H 5250 3550 50  0001 C CNN
	1    5250 3550
	0    -1   -1   0   
$EndComp
$Comp
L Switch:SW_Push SW30
U 1 1 5F4A24E2
P 5250 1400
F 0 "SW30" V 5250 1350 50  0000 R CNN
F 1 "Cherry MX" V 5200 1300 50  0001 R CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX_1.00u_PCB" H 5250 1600 50  0001 C CNN
F 3 "~" H 5250 1600 50  0001 C CNN
	1    5250 1400
	0    -1   -1   0   
$EndComp
$Comp
L Diode:1N4148 D29
U 1 1 5F4A24E8
P 5250 1750
F 0 "D29" V 5350 1650 50  0000 C CNN
F 1 "1N4148" V 5350 1950 50  0000 C CNN
F 2 "Diode_THT:D_DO-35_SOD27_P7.62mm_Horizontal" H 5250 1575 50  0001 C CNN
F 3 "https://assets.nexperia.com/documents/data-sheet/1N4148_1N4448.pdf" H 5250 1750 50  0001 C CNN
	1    5250 1750
	0    -1   -1   0   
$EndComp
$Comp
L Diode:1N4148 D30
U 1 1 5F4A24EE
P 5250 2650
F 0 "D30" V 5350 2550 50  0000 C CNN
F 1 "1N4148" V 5350 2850 50  0000 C CNN
F 2 "Diode_THT:D_DO-35_SOD27_P7.62mm_Horizontal" H 5250 2475 50  0001 C CNN
F 3 "https://assets.nexperia.com/documents/data-sheet/1N4148_1N4448.pdf" H 5250 2650 50  0001 C CNN
	1    5250 2650
	0    -1   -1   0   
$EndComp
$Comp
L Switch:SW_Push SW31
U 1 1 5F4A24F4
P 5250 2300
F 0 "SW31" V 5250 2250 50  0000 R CNN
F 1 "Cherry MX" V 5200 2200 50  0001 R CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX_1.00u_PCB" H 5250 2500 50  0001 C CNN
F 3 "~" H 5250 2500 50  0001 C CNN
	1    5250 2300
	0    -1   -1   0   
$EndComp
Wire Wire Line
	5250 2000 5550 2000
Wire Wire Line
	5550 2000 5550 2900
Wire Wire Line
	5250 4700 5550 4700
Connection ~ 5550 4700
Wire Wire Line
	5250 3800 5550 3800
Connection ~ 5550 3800
Wire Wire Line
	5550 3800 5550 4700
Wire Wire Line
	5250 2900 5550 2900
Connection ~ 5550 2900
Wire Wire Line
	5550 2900 5550 3800
Wire Wire Line
	4550 3900 5250 3900
Wire Wire Line
	4550 3000 5250 3000
Wire Wire Line
	4550 2100 5250 2100
Wire Wire Line
	4550 1200 5250 1200
Text Notes 4950 2350 0    50   ~ 0
yY
Text Notes 4950 1450 0    50   ~ 0
6^
Wire Wire Line
	5950 1900 5950 2000
Wire Wire Line
	5950 2900 5950 2800
$Comp
L Switch:SW_Push SW36
U 1 1 5F4A2510
P 5950 3200
F 0 "SW36" V 5950 3150 50  0000 R CNN
F 1 "Cherry MX" V 5900 3100 50  0001 R CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX_1.00u_PCB" H 5950 3400 50  0001 C CNN
F 3 "~" H 5950 3400 50  0001 C CNN
	1    5950 3200
	0    -1   -1   0   
$EndComp
Wire Wire Line
	5950 3800 5950 3700
Text Notes 5650 3250 0    50   ~ 0
jJ
$Comp
L Diode:1N4148 D36
U 1 1 5F4A2518
P 5950 4450
F 0 "D36" V 6050 4350 50  0000 C CNN
F 1 "1N4148" V 6050 4650 50  0000 C CNN
F 2 "Diode_THT:D_DO-35_SOD27_P7.62mm_Horizontal" H 5950 4275 50  0001 C CNN
F 3 "https://assets.nexperia.com/documents/data-sheet/1N4148_1N4448.pdf" H 5950 4450 50  0001 C CNN
	1    5950 4450
	0    -1   -1   0   
$EndComp
$Comp
L Switch:SW_Push SW37
U 1 1 5F4A251E
P 5950 4100
F 0 "SW37" V 5950 4050 50  0000 R CNN
F 1 "Cherry MX" V 5900 4000 50  0001 R CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX_1.00u_PCB" H 5950 4300 50  0001 C CNN
F 3 "~" H 5950 4300 50  0001 C CNN
	1    5950 4100
	0    -1   -1   0   
$EndComp
Wire Wire Line
	5950 4700 5950 4600
Text Notes 5650 4150 0    50   ~ 0
mM
$Comp
L Diode:1N4148 D35
U 1 1 5F4A2533
P 5950 3550
F 0 "D35" V 6050 3450 50  0000 C CNN
F 1 "1N4148" V 6050 3750 50  0000 C CNN
F 2 "Diode_THT:D_DO-35_SOD27_P7.62mm_Horizontal" H 5950 3375 50  0001 C CNN
F 3 "https://assets.nexperia.com/documents/data-sheet/1N4148_1N4448.pdf" H 5950 3550 50  0001 C CNN
	1    5950 3550
	0    -1   -1   0   
$EndComp
$Comp
L Switch:SW_Push SW34
U 1 1 5F4A2539
P 5950 1400
F 0 "SW34" V 5950 1350 50  0000 R CNN
F 1 "Cherry MX" V 5900 1300 50  0001 R CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX_1.00u_PCB" H 5950 1600 50  0001 C CNN
F 3 "~" H 5950 1600 50  0001 C CNN
	1    5950 1400
	0    -1   -1   0   
$EndComp
$Comp
L Diode:1N4148 D33
U 1 1 5F4A253F
P 5950 1750
F 0 "D33" V 6050 1650 50  0000 C CNN
F 1 "1N4148" V 6050 1950 50  0000 C CNN
F 2 "Diode_THT:D_DO-35_SOD27_P7.62mm_Horizontal" H 5950 1575 50  0001 C CNN
F 3 "https://assets.nexperia.com/documents/data-sheet/1N4148_1N4448.pdf" H 5950 1750 50  0001 C CNN
	1    5950 1750
	0    -1   -1   0   
$EndComp
$Comp
L Diode:1N4148 D34
U 1 1 5F4A2545
P 5950 2650
F 0 "D34" V 6050 2550 50  0000 C CNN
F 1 "1N4148" V 6050 2850 50  0000 C CNN
F 2 "Diode_THT:D_DO-35_SOD27_P7.62mm_Horizontal" H 5950 2475 50  0001 C CNN
F 3 "https://assets.nexperia.com/documents/data-sheet/1N4148_1N4448.pdf" H 5950 2650 50  0001 C CNN
	1    5950 2650
	0    -1   -1   0   
$EndComp
$Comp
L Switch:SW_Push SW35
U 1 1 5F4A254B
P 5950 2300
F 0 "SW35" V 5950 2250 50  0000 R CNN
F 1 "Cherry MX" V 5900 2200 50  0001 R CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX_1.00u_PCB" H 5950 2500 50  0001 C CNN
F 3 "~" H 5950 2500 50  0001 C CNN
	1    5950 2300
	0    -1   -1   0   
$EndComp
Wire Wire Line
	5950 2000 6250 2000
Wire Wire Line
	6250 2000 6250 2900
Wire Wire Line
	5950 4700 6250 4700
Connection ~ 6250 4700
Wire Wire Line
	5950 3800 6250 3800
Connection ~ 6250 3800
Wire Wire Line
	6250 3800 6250 4700
Wire Wire Line
	5950 2900 6250 2900
Connection ~ 6250 2900
Wire Wire Line
	6250 2900 6250 3800
Wire Wire Line
	5250 3900 5950 3900
Wire Wire Line
	5250 3000 5950 3000
Wire Wire Line
	5250 2100 5950 2100
Wire Wire Line
	5250 1200 5950 1200
Text Notes 5650 2350 0    50   ~ 0
uU
Text Notes 5650 1450 0    50   ~ 0
7&
Wire Wire Line
	6650 1900 6650 2000
Wire Wire Line
	6650 2900 6650 2800
$Comp
L Switch:SW_Push SW40
U 1 1 5F4A2567
P 6650 3200
F 0 "SW40" V 6650 3150 50  0000 R CNN
F 1 "Cherry MX" V 6600 3100 50  0001 R CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX_1.00u_PCB" H 6650 3400 50  0001 C CNN
F 3 "~" H 6650 3400 50  0001 C CNN
	1    6650 3200
	0    -1   -1   0   
$EndComp
Wire Wire Line
	6650 3800 6650 3700
Text Notes 6350 3250 0    50   ~ 0
kK
$Comp
L Diode:1N4148 D40
U 1 1 5F4A256F
P 6650 4450
F 0 "D40" V 6750 4350 50  0000 C CNN
F 1 "1N4148" V 6750 4650 50  0000 C CNN
F 2 "Diode_THT:D_DO-35_SOD27_P7.62mm_Horizontal" H 6650 4275 50  0001 C CNN
F 3 "https://assets.nexperia.com/documents/data-sheet/1N4148_1N4448.pdf" H 6650 4450 50  0001 C CNN
	1    6650 4450
	0    -1   -1   0   
$EndComp
$Comp
L Switch:SW_Push SW41
U 1 1 5F4A2575
P 6650 4100
F 0 "SW41" V 6650 4050 50  0000 R CNN
F 1 "Cherry MX" V 6600 4000 50  0001 R CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX_1.00u_PCB" H 6650 4300 50  0001 C CNN
F 3 "~" H 6650 4300 50  0001 C CNN
	1    6650 4100
	0    -1   -1   0   
$EndComp
Wire Wire Line
	6650 4700 6650 4600
Text Notes 6350 4150 0    50   ~ 0
,<
$Comp
L Diode:1N4148 D39
U 1 1 5F4A258A
P 6650 3550
F 0 "D39" V 6750 3450 50  0000 C CNN
F 1 "1N4148" V 6750 3750 50  0000 C CNN
F 2 "Diode_THT:D_DO-35_SOD27_P7.62mm_Horizontal" H 6650 3375 50  0001 C CNN
F 3 "https://assets.nexperia.com/documents/data-sheet/1N4148_1N4448.pdf" H 6650 3550 50  0001 C CNN
	1    6650 3550
	0    -1   -1   0   
$EndComp
$Comp
L Switch:SW_Push SW38
U 1 1 5F4A2590
P 6650 1400
F 0 "SW38" V 6650 1350 50  0000 R CNN
F 1 "Cherry MX" V 6600 1300 50  0001 R CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX_1.00u_PCB" H 6650 1600 50  0001 C CNN
F 3 "~" H 6650 1600 50  0001 C CNN
	1    6650 1400
	0    -1   -1   0   
$EndComp
$Comp
L Diode:1N4148 D37
U 1 1 5F4A2596
P 6650 1750
F 0 "D37" V 6750 1650 50  0000 C CNN
F 1 "1N4148" V 6750 1950 50  0000 C CNN
F 2 "Diode_THT:D_DO-35_SOD27_P7.62mm_Horizontal" H 6650 1575 50  0001 C CNN
F 3 "https://assets.nexperia.com/documents/data-sheet/1N4148_1N4448.pdf" H 6650 1750 50  0001 C CNN
	1    6650 1750
	0    -1   -1   0   
$EndComp
$Comp
L Diode:1N4148 D38
U 1 1 5F4A259C
P 6650 2650
F 0 "D38" V 6750 2550 50  0000 C CNN
F 1 "1N4148" V 6750 2850 50  0000 C CNN
F 2 "Diode_THT:D_DO-35_SOD27_P7.62mm_Horizontal" H 6650 2475 50  0001 C CNN
F 3 "https://assets.nexperia.com/documents/data-sheet/1N4148_1N4448.pdf" H 6650 2650 50  0001 C CNN
	1    6650 2650
	0    -1   -1   0   
$EndComp
$Comp
L Switch:SW_Push SW39
U 1 1 5F4A25A2
P 6650 2300
F 0 "SW39" V 6650 2250 50  0000 R CNN
F 1 "Cherry MX" V 6600 2200 50  0001 R CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX_1.00u_PCB" H 6650 2500 50  0001 C CNN
F 3 "~" H 6650 2500 50  0001 C CNN
	1    6650 2300
	0    -1   -1   0   
$EndComp
Wire Wire Line
	6650 2000 6950 2000
Wire Wire Line
	6950 2000 6950 2900
Wire Wire Line
	6650 4700 6950 4700
Connection ~ 6950 4700
Wire Wire Line
	6650 3800 6950 3800
Connection ~ 6950 3800
Wire Wire Line
	6950 3800 6950 4700
Wire Wire Line
	6650 2900 6950 2900
Connection ~ 6950 2900
Wire Wire Line
	6950 2900 6950 3800
Wire Wire Line
	5950 3900 6650 3900
Wire Wire Line
	5950 3000 6650 3000
Wire Wire Line
	5950 2100 6650 2100
Wire Wire Line
	5950 1200 6650 1200
Text Notes 6350 2350 0    50   ~ 0
iI
Text Notes 6350 1450 0    50   ~ 0
8*
Wire Wire Line
	7350 1900 7350 2000
Wire Wire Line
	7350 2900 7350 2800
$Comp
L Switch:SW_Push SW44
U 1 1 5F4EFC63
P 7350 3200
F 0 "SW44" V 7350 3150 50  0000 R CNN
F 1 "Cherry MX" V 7300 3100 50  0001 R CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX_1.00u_PCB" H 7350 3400 50  0001 C CNN
F 3 "~" H 7350 3400 50  0001 C CNN
	1    7350 3200
	0    -1   -1   0   
$EndComp
Wire Wire Line
	7350 3800 7350 3700
Text Notes 7050 3250 0    50   ~ 0
lL
$Comp
L Diode:1N4148 D44
U 1 1 5F4EFC6B
P 7350 4450
F 0 "D44" V 7450 4350 50  0000 C CNN
F 1 "1N4148" V 7450 4650 50  0000 C CNN
F 2 "Diode_THT:D_DO-35_SOD27_P7.62mm_Horizontal" H 7350 4275 50  0001 C CNN
F 3 "https://assets.nexperia.com/documents/data-sheet/1N4148_1N4448.pdf" H 7350 4450 50  0001 C CNN
	1    7350 4450
	0    -1   -1   0   
$EndComp
$Comp
L Switch:SW_Push SW45
U 1 1 5F4EFC71
P 7350 4100
F 0 "SW45" V 7350 4050 50  0000 R CNN
F 1 "Cherry MX" V 7300 4000 50  0001 R CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX_1.00u_PCB" H 7350 4300 50  0001 C CNN
F 3 "~" H 7350 4300 50  0001 C CNN
	1    7350 4100
	0    -1   -1   0   
$EndComp
Wire Wire Line
	7350 4700 7350 4600
Text Notes 7050 4150 0    50   ~ 0
.>
$Comp
L Diode:1N4148 D45
U 1 1 5F4EFC79
P 7350 5350
F 0 "D45" V 7450 5250 50  0000 C CNN
F 1 "1N4148" V 7450 5550 50  0000 C CNN
F 2 "Diode_THT:D_DO-35_SOD27_P7.62mm_Horizontal" H 7350 5175 50  0001 C CNN
F 3 "https://assets.nexperia.com/documents/data-sheet/1N4148_1N4448.pdf" H 7350 5350 50  0001 C CNN
	1    7350 5350
	0    -1   -1   0   
$EndComp
$Comp
L Switch:SW_Push SW46
U 1 1 5F4EFC7F
P 7350 5000
F 0 "SW46" V 7350 4950 50  0000 R CNN
F 1 "Cherry MX" V 7300 4900 50  0001 R CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX_1.00u_PCB" H 7350 5200 50  0001 C CNN
F 3 "~" H 7350 5200 50  0001 C CNN
	1    7350 5000
	0    -1   -1   0   
$EndComp
Wire Wire Line
	7350 5600 7350 5500
$Comp
L Diode:1N4148 D43
U 1 1 5F4EFC86
P 7350 3550
F 0 "D43" V 7450 3450 50  0000 C CNN
F 1 "1N4148" V 7450 3750 50  0000 C CNN
F 2 "Diode_THT:D_DO-35_SOD27_P7.62mm_Horizontal" H 7350 3375 50  0001 C CNN
F 3 "https://assets.nexperia.com/documents/data-sheet/1N4148_1N4448.pdf" H 7350 3550 50  0001 C CNN
	1    7350 3550
	0    -1   -1   0   
$EndComp
$Comp
L Switch:SW_Push SW42
U 1 1 5F4EFC8C
P 7350 1400
F 0 "SW42" V 7350 1350 50  0000 R CNN
F 1 "Cherry MX" V 7300 1300 50  0001 R CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX_1.00u_PCB" H 7350 1600 50  0001 C CNN
F 3 "~" H 7350 1600 50  0001 C CNN
	1    7350 1400
	0    -1   -1   0   
$EndComp
$Comp
L Diode:1N4148 D41
U 1 1 5F4EFC92
P 7350 1750
F 0 "D41" V 7450 1650 50  0000 C CNN
F 1 "1N4148" V 7450 1950 50  0000 C CNN
F 2 "Diode_THT:D_DO-35_SOD27_P7.62mm_Horizontal" H 7350 1575 50  0001 C CNN
F 3 "https://assets.nexperia.com/documents/data-sheet/1N4148_1N4448.pdf" H 7350 1750 50  0001 C CNN
	1    7350 1750
	0    -1   -1   0   
$EndComp
$Comp
L Diode:1N4148 D42
U 1 1 5F4EFC98
P 7350 2650
F 0 "D42" V 7450 2550 50  0000 C CNN
F 1 "1N4148" V 7450 2850 50  0000 C CNN
F 2 "Diode_THT:D_DO-35_SOD27_P7.62mm_Horizontal" H 7350 2475 50  0001 C CNN
F 3 "https://assets.nexperia.com/documents/data-sheet/1N4148_1N4448.pdf" H 7350 2650 50  0001 C CNN
	1    7350 2650
	0    -1   -1   0   
$EndComp
$Comp
L Switch:SW_Push SW43
U 1 1 5F4EFC9E
P 7350 2300
F 0 "SW43" V 7350 2250 50  0000 R CNN
F 1 "Cherry MX" V 7300 2200 50  0001 R CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX_1.00u_PCB" H 7350 2500 50  0001 C CNN
F 3 "~" H 7350 2500 50  0001 C CNN
	1    7350 2300
	0    -1   -1   0   
$EndComp
Wire Wire Line
	7350 2000 7650 2000
Wire Wire Line
	7650 5600 7350 5600
Wire Wire Line
	7650 2000 7650 2900
Wire Wire Line
	7350 4700 7650 4700
Connection ~ 7650 4700
Wire Wire Line
	7650 4700 7650 5600
Wire Wire Line
	7350 3800 7650 3800
Connection ~ 7650 3800
Wire Wire Line
	7650 3800 7650 4700
Wire Wire Line
	7350 2900 7650 2900
Connection ~ 7650 2900
Wire Wire Line
	7650 2900 7650 3800
Wire Wire Line
	6650 3900 7350 3900
Wire Wire Line
	6650 3000 7350 3000
Wire Wire Line
	6650 2100 7350 2100
Wire Wire Line
	6650 1200 7350 1200
Text Notes 7050 2350 0    50   ~ 0
oO
Text Notes 7050 1450 0    50   ~ 0
9(
Text Notes 7000 5050 0    50   ~ 0
Right\nAlt
Wire Wire Line
	8050 1900 8050 2000
Wire Wire Line
	8050 2900 8050 2800
$Comp
L Switch:SW_Push SW49
U 1 1 5F4EFCBA
P 8050 3200
F 0 "SW49" V 8050 3150 50  0000 R CNN
F 1 "Cherry MX" V 8000 3100 50  0001 R CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX_1.00u_PCB" H 8050 3400 50  0001 C CNN
F 3 "~" H 8050 3400 50  0001 C CNN
	1    8050 3200
	0    -1   -1   0   
$EndComp
Wire Wire Line
	8050 3800 8050 3700
$Comp
L Diode:1N4148 D49
U 1 1 5F4EFCC1
P 8050 4450
F 0 "D49" V 8150 4350 50  0000 C CNN
F 1 "1N4148" V 8150 4650 50  0000 C CNN
F 2 "Diode_THT:D_DO-35_SOD27_P7.62mm_Horizontal" H 8050 4275 50  0001 C CNN
F 3 "https://assets.nexperia.com/documents/data-sheet/1N4148_1N4448.pdf" H 8050 4450 50  0001 C CNN
	1    8050 4450
	0    -1   -1   0   
$EndComp
$Comp
L Switch:SW_Push SW50
U 1 1 5F4EFCC7
P 8050 4100
F 0 "SW50" V 8050 4050 50  0000 R CNN
F 1 "Cherry MX" V 8000 4000 50  0001 R CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX_1.00u_PCB" H 8050 4300 50  0001 C CNN
F 3 "~" H 8050 4300 50  0001 C CNN
	1    8050 4100
	0    -1   -1   0   
$EndComp
Wire Wire Line
	8050 4700 8050 4600
$Comp
L Diode:1N4148 D50
U 1 1 5F4EFCCE
P 8050 5350
F 0 "D50" V 8150 5250 50  0000 C CNN
F 1 "1N4148" V 8150 5550 50  0000 C CNN
F 2 "Diode_THT:D_DO-35_SOD27_P7.62mm_Horizontal" H 8050 5175 50  0001 C CNN
F 3 "https://assets.nexperia.com/documents/data-sheet/1N4148_1N4448.pdf" H 8050 5350 50  0001 C CNN
	1    8050 5350
	0    -1   -1   0   
$EndComp
$Comp
L Switch:SW_Push SW51
U 1 1 5F4EFCD4
P 8050 5000
F 0 "SW51" V 8050 4950 50  0000 R CNN
F 1 "Cherry MX" V 8000 4900 50  0001 R CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX_1.00u_PCB" H 8050 5200 50  0001 C CNN
F 3 "~" H 8050 5200 50  0001 C CNN
	1    8050 5000
	0    -1   -1   0   
$EndComp
Wire Wire Line
	8050 5600 8050 5500
$Comp
L Diode:1N4148 D48
U 1 1 5F4EFCDB
P 8050 3550
F 0 "D48" V 8150 3450 50  0000 C CNN
F 1 "1N4148" V 8150 3750 50  0000 C CNN
F 2 "Diode_THT:D_DO-35_SOD27_P7.62mm_Horizontal" H 8050 3375 50  0001 C CNN
F 3 "https://assets.nexperia.com/documents/data-sheet/1N4148_1N4448.pdf" H 8050 3550 50  0001 C CNN
	1    8050 3550
	0    -1   -1   0   
$EndComp
$Comp
L Switch:SW_Push SW47
U 1 1 5F4EFCE1
P 8050 1400
F 0 "SW47" V 8050 1350 50  0000 R CNN
F 1 "Cherry MX" V 8000 1300 50  0001 R CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX_1.00u_PCB" H 8050 1600 50  0001 C CNN
F 3 "~" H 8050 1600 50  0001 C CNN
	1    8050 1400
	0    -1   -1   0   
$EndComp
$Comp
L Diode:1N4148 D46
U 1 1 5F4EFCE7
P 8050 1750
F 0 "D46" V 8150 1650 50  0000 C CNN
F 1 "1N4148" V 8150 1950 50  0000 C CNN
F 2 "Diode_THT:D_DO-35_SOD27_P7.62mm_Horizontal" H 8050 1575 50  0001 C CNN
F 3 "https://assets.nexperia.com/documents/data-sheet/1N4148_1N4448.pdf" H 8050 1750 50  0001 C CNN
	1    8050 1750
	0    -1   -1   0   
$EndComp
$Comp
L Diode:1N4148 D47
U 1 1 5F4EFCED
P 8050 2650
F 0 "D47" V 8150 2550 50  0000 C CNN
F 1 "1N4148" V 8150 2850 50  0000 C CNN
F 2 "Diode_THT:D_DO-35_SOD27_P7.62mm_Horizontal" H 8050 2475 50  0001 C CNN
F 3 "https://assets.nexperia.com/documents/data-sheet/1N4148_1N4448.pdf" H 8050 2650 50  0001 C CNN
	1    8050 2650
	0    -1   -1   0   
$EndComp
$Comp
L Switch:SW_Push SW48
U 1 1 5F4EFCF3
P 8050 2300
F 0 "SW48" V 8050 2250 50  0000 R CNN
F 1 "Cherry MX" V 8000 2200 50  0001 R CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX_1.00u_PCB" H 8050 2500 50  0001 C CNN
F 3 "~" H 8050 2500 50  0001 C CNN
	1    8050 2300
	0    -1   -1   0   
$EndComp
Wire Wire Line
	8050 2000 8350 2000
Wire Wire Line
	8350 5600 8050 5600
Wire Wire Line
	8350 2000 8350 2900
Wire Wire Line
	8050 4700 8350 4700
Connection ~ 8350 4700
Wire Wire Line
	8350 4700 8350 5600
Wire Wire Line
	8050 3800 8350 3800
Connection ~ 8350 3800
Wire Wire Line
	8350 3800 8350 4700
Wire Wire Line
	8050 2900 8350 2900
Connection ~ 8350 2900
Wire Wire Line
	8350 2900 8350 3800
Wire Wire Line
	7350 4800 8050 4800
Wire Wire Line
	7350 3900 8050 3900
Wire Wire Line
	7350 3000 8050 3000
Wire Wire Line
	7350 2100 8050 2100
Wire Wire Line
	7350 1200 8050 1200
Text Notes 7750 2350 0    50   ~ 0
pP
Text Notes 7700 5050 0    50   ~ 0
Right\nCtrl
Wire Wire Line
	8750 1900 8750 2000
Wire Wire Line
	8750 2900 8750 2800
$Comp
L Switch:SW_Push SW54
U 1 1 5F4EFD0E
P 8750 3200
F 0 "SW54" V 8750 3150 50  0000 R CNN
F 1 "Cherry MX" V 8700 3100 50  0001 R CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX_1.00u_PCB" H 8750 3400 50  0001 C CNN
F 3 "~" H 8750 3400 50  0001 C CNN
	1    8750 3200
	0    -1   -1   0   
$EndComp
Wire Wire Line
	8750 3800 8750 3700
Text Notes 8500 3250 0    50   ~ 0
'"
$Comp
L Diode:1N4148 D54
U 1 1 5F4EFD24
P 8750 5350
F 0 "D54" V 8850 5250 50  0000 C CNN
F 1 "1N4148" V 8850 5550 50  0000 C CNN
F 2 "Diode_THT:D_DO-35_SOD27_P7.62mm_Horizontal" H 8750 5175 50  0001 C CNN
F 3 "https://assets.nexperia.com/documents/data-sheet/1N4148_1N4448.pdf" H 8750 5350 50  0001 C CNN
	1    8750 5350
	0    -1   -1   0   
$EndComp
$Comp
L Switch:SW_Push SW55
U 1 1 5F4EFD2A
P 8750 5000
F 0 "SW55" V 8750 4950 50  0000 R CNN
F 1 "Cherry MX" V 8700 4900 50  0001 R CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX_1.00u_PCB" H 8750 5200 50  0001 C CNN
F 3 "~" H 8750 5200 50  0001 C CNN
	1    8750 5000
	0    -1   -1   0   
$EndComp
Wire Wire Line
	8750 5600 8750 5500
$Comp
L Diode:1N4148 D53
U 1 1 5F4EFD31
P 8750 3550
F 0 "D53" V 8850 3450 50  0000 C CNN
F 1 "1N4148" V 8850 3750 50  0000 C CNN
F 2 "Diode_THT:D_DO-35_SOD27_P7.62mm_Horizontal" H 8750 3375 50  0001 C CNN
F 3 "https://assets.nexperia.com/documents/data-sheet/1N4148_1N4448.pdf" H 8750 3550 50  0001 C CNN
	1    8750 3550
	0    -1   -1   0   
$EndComp
$Comp
L Switch:SW_Push SW52
U 1 1 5F4EFD37
P 8750 1400
F 0 "SW52" V 8750 1350 50  0000 R CNN
F 1 "Cherry MX" V 8700 1300 50  0001 R CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX_1.00u_PCB" H 8750 1600 50  0001 C CNN
F 3 "~" H 8750 1600 50  0001 C CNN
	1    8750 1400
	0    -1   -1   0   
$EndComp
$Comp
L Diode:1N4148 D51
U 1 1 5F4EFD3D
P 8750 1750
F 0 "D51" V 8850 1650 50  0000 C CNN
F 1 "1N4148" V 8850 1950 50  0000 C CNN
F 2 "Diode_THT:D_DO-35_SOD27_P7.62mm_Horizontal" H 8750 1575 50  0001 C CNN
F 3 "https://assets.nexperia.com/documents/data-sheet/1N4148_1N4448.pdf" H 8750 1750 50  0001 C CNN
	1    8750 1750
	0    -1   -1   0   
$EndComp
$Comp
L Diode:1N4148 D52
U 1 1 5F4EFD43
P 8750 2650
F 0 "D52" V 8850 2550 50  0000 C CNN
F 1 "1N4148" V 8850 2850 50  0000 C CNN
F 2 "Diode_THT:D_DO-35_SOD27_P7.62mm_Horizontal" H 8750 2475 50  0001 C CNN
F 3 "https://assets.nexperia.com/documents/data-sheet/1N4148_1N4448.pdf" H 8750 2650 50  0001 C CNN
	1    8750 2650
	0    -1   -1   0   
$EndComp
$Comp
L Switch:SW_Push SW53
U 1 1 5F4EFD49
P 8750 2300
F 0 "SW53" V 8750 2250 50  0000 R CNN
F 1 "Cherry MX" V 8700 2200 50  0001 R CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX_1.00u_PCB" H 8750 2500 50  0001 C CNN
F 3 "~" H 8750 2500 50  0001 C CNN
	1    8750 2300
	0    -1   -1   0   
$EndComp
Wire Wire Line
	8750 2000 9050 2000
Wire Wire Line
	9050 5600 8750 5600
Wire Wire Line
	9050 2000 9050 2900
Wire Wire Line
	8750 3800 9050 3800
Connection ~ 9050 3800
Wire Wire Line
	8750 2900 9050 2900
Connection ~ 9050 2900
Wire Wire Line
	9050 2900 9050 3800
Wire Wire Line
	8050 4800 8750 4800
Wire Wire Line
	8050 3000 8750 3000
Wire Wire Line
	8050 2100 8750 2100
Wire Wire Line
	8050 1200 8750 1200
Text Notes 8450 2350 0    50   ~ 0
[{
Text Notes 8450 1450 0    50   ~ 0
-_
Text Notes 8450 5050 0    50   ~ 0
Fn
Wire Wire Line
	9450 1900 9450 2000
Wire Wire Line
	9450 2900 9450 2800
$Comp
L Diode:1N4148 D57
U 1 1 5F4EFD6D
P 9450 4450
F 0 "D57" V 9550 4350 50  0000 C CNN
F 1 "1N4148" V 9550 4650 50  0000 C CNN
F 2 "Diode_THT:D_DO-35_SOD27_P7.62mm_Horizontal" H 9450 4275 50  0001 C CNN
F 3 "https://assets.nexperia.com/documents/data-sheet/1N4148_1N4448.pdf" H 9450 4450 50  0001 C CNN
	1    9450 4450
	0    -1   -1   0   
$EndComp
$Comp
L Switch:SW_Push SW58
U 1 1 5F4EFD73
P 9450 4100
F 0 "SW58" V 9450 4050 50  0000 R CNN
F 1 "Cherry MX" V 9400 4000 50  0001 R CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX_1.75u_PCB" H 9450 4300 50  0001 C CNN
F 3 "~" H 9450 4300 50  0001 C CNN
	1    9450 4100
	0    -1   -1   0   
$EndComp
Wire Wire Line
	9450 4700 9450 4600
Text Notes 9100 4150 0    50   ~ 0
Right\nShift
$Comp
L Diode:1N4148 D58
U 1 1 5F4EFD7B
P 9450 5350
F 0 "D58" V 9550 5250 50  0000 C CNN
F 1 "1N4148" V 9550 5550 50  0000 C CNN
F 2 "Diode_THT:D_DO-35_SOD27_P7.62mm_Horizontal" H 9450 5175 50  0001 C CNN
F 3 "https://assets.nexperia.com/documents/data-sheet/1N4148_1N4448.pdf" H 9450 5350 50  0001 C CNN
	1    9450 5350
	0    -1   -1   0   
$EndComp
$Comp
L Switch:SW_Push SW59
U 1 1 5F4EFD81
P 9450 5000
F 0 "SW59" V 9450 4950 50  0000 R CNN
F 1 "Cherry MX" V 9400 4900 50  0001 R CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX_1.00u_PCB" H 9450 5200 50  0001 C CNN
F 3 "~" H 9450 5200 50  0001 C CNN
	1    9450 5000
	0    -1   -1   0   
$EndComp
Wire Wire Line
	9450 5600 9450 5500
$Comp
L Switch:SW_Push SW56
U 1 1 5F4EFD8E
P 9450 1400
F 0 "SW56" V 9450 1350 50  0000 R CNN
F 1 "Cherry MX" V 9400 1300 50  0001 R CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX_1.00u_PCB" H 9450 1600 50  0001 C CNN
F 3 "~" H 9450 1600 50  0001 C CNN
	1    9450 1400
	0    -1   -1   0   
$EndComp
$Comp
L Diode:1N4148 D55
U 1 1 5F4EFD94
P 9450 1750
F 0 "D55" V 9550 1650 50  0000 C CNN
F 1 "1N4148" V 9550 1950 50  0000 C CNN
F 2 "Diode_THT:D_DO-35_SOD27_P7.62mm_Horizontal" H 9450 1575 50  0001 C CNN
F 3 "https://assets.nexperia.com/documents/data-sheet/1N4148_1N4448.pdf" H 9450 1750 50  0001 C CNN
	1    9450 1750
	0    -1   -1   0   
$EndComp
$Comp
L Diode:1N4148 D56
U 1 1 5F4EFD9A
P 9450 2650
F 0 "D56" V 9550 2550 50  0000 C CNN
F 1 "1N4148" V 9550 2850 50  0000 C CNN
F 2 "Diode_THT:D_DO-35_SOD27_P7.62mm_Horizontal" H 9450 2475 50  0001 C CNN
F 3 "https://assets.nexperia.com/documents/data-sheet/1N4148_1N4448.pdf" H 9450 2650 50  0001 C CNN
	1    9450 2650
	0    -1   -1   0   
$EndComp
$Comp
L Switch:SW_Push SW57
U 1 1 5F4EFDA0
P 9450 2300
F 0 "SW57" V 9450 2250 50  0000 R CNN
F 1 "Cherry MX" V 9400 2200 50  0001 R CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX_1.00u_PCB" H 9450 2500 50  0001 C CNN
F 3 "~" H 9450 2500 50  0001 C CNN
	1    9450 2300
	0    -1   -1   0   
$EndComp
Wire Wire Line
	9450 2000 9750 2000
Wire Wire Line
	9750 5600 9450 5600
Wire Wire Line
	9750 2000 9750 2900
Wire Wire Line
	9450 4700 9750 4700
Connection ~ 9750 4700
Wire Wire Line
	9750 4700 9750 5600
Wire Wire Line
	9450 2900 9750 2900
Connection ~ 9750 2900
Wire Wire Line
	8750 4800 9450 4800
Wire Wire Line
	8750 2100 9450 2100
Wire Wire Line
	8750 1200 9450 1200
Text Notes 9150 2350 0    50   ~ 0
]}
Text Notes 9150 1450 0    50   ~ 0
=+
Text Notes 9100 5050 0    50   ~ 0
Left\nArrow
Wire Wire Line
	10150 1900 10150 2000
Wire Wire Line
	10150 2900 10150 2800
$Comp
L Switch:SW_Push SW62
U 1 1 5F4EFDBC
P 10150 3200
F 0 "SW62" V 10150 3150 50  0000 R CNN
F 1 "Cherry MX" V 10100 3100 50  0001 R CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX_2.25u_PCB" H 10150 3400 50  0001 C CNN
F 3 "~" H 10150 3400 50  0001 C CNN
	1    10150 3200
	0    -1   -1   0   
$EndComp
Wire Wire Line
	10150 3800 10150 3700
Text Notes 9800 3200 0    50   ~ 0
Enter
$Comp
L Diode:1N4148 D62
U 1 1 5F4EFDC4
P 10150 4450
F 0 "D62" V 10250 4350 50  0000 C CNN
F 1 "1N4148" V 10250 4650 50  0000 C CNN
F 2 "Diode_THT:D_DO-35_SOD27_P7.62mm_Horizontal" H 10150 4275 50  0001 C CNN
F 3 "https://assets.nexperia.com/documents/data-sheet/1N4148_1N4448.pdf" H 10150 4450 50  0001 C CNN
	1    10150 4450
	0    -1   -1   0   
$EndComp
$Comp
L Switch:SW_Push SW63
U 1 1 5F4EFDCA
P 10150 4100
F 0 "SW63" V 10150 4050 50  0000 R CNN
F 1 "Cherry MX" V 10100 4000 50  0001 R CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX_1.00u_PCB" H 10150 4300 50  0001 C CNN
F 3 "~" H 10150 4300 50  0001 C CNN
	1    10150 4100
	0    -1   -1   0   
$EndComp
Wire Wire Line
	10150 4700 10150 4600
Text Notes 9800 4150 0    50   ~ 0
Up\nArrow
$Comp
L Diode:1N4148 D63
U 1 1 5F4EFDD2
P 10150 5350
F 0 "D63" V 10250 5250 50  0000 C CNN
F 1 "1N4148" V 10250 5550 50  0000 C CNN
F 2 "Diode_THT:D_DO-35_SOD27_P7.62mm_Horizontal" H 10150 5175 50  0001 C CNN
F 3 "https://assets.nexperia.com/documents/data-sheet/1N4148_1N4448.pdf" H 10150 5350 50  0001 C CNN
	1    10150 5350
	0    -1   -1   0   
$EndComp
$Comp
L Switch:SW_Push SW64
U 1 1 5F4EFDD8
P 10150 5000
F 0 "SW64" V 10150 4950 50  0000 R CNN
F 1 "Cherry MX" V 10100 4900 50  0001 R CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX_1.00u_PCB" H 10150 5200 50  0001 C CNN
F 3 "~" H 10150 5200 50  0001 C CNN
	1    10150 5000
	0    -1   -1   0   
$EndComp
Wire Wire Line
	10150 5600 10150 5500
$Comp
L Diode:1N4148 D61
U 1 1 5F4EFDDF
P 10150 3550
F 0 "D61" V 10250 3450 50  0000 C CNN
F 1 "1N4148" V 10250 3750 50  0000 C CNN
F 2 "Diode_THT:D_DO-35_SOD27_P7.62mm_Horizontal" H 10150 3375 50  0001 C CNN
F 3 "https://assets.nexperia.com/documents/data-sheet/1N4148_1N4448.pdf" H 10150 3550 50  0001 C CNN
	1    10150 3550
	0    -1   -1   0   
$EndComp
$Comp
L Switch:SW_Push SW60
U 1 1 5F4EFDE5
P 10150 1400
F 0 "SW60" V 10150 1350 50  0000 R CNN
F 1 "Cherry MX" V 10100 1300 50  0001 R CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX_2.00u_PCB" H 10150 1600 50  0001 C CNN
F 3 "~" H 10150 1600 50  0001 C CNN
	1    10150 1400
	0    -1   -1   0   
$EndComp
$Comp
L Diode:1N4148 D59
U 1 1 5F4EFDEB
P 10150 1750
F 0 "D59" V 10250 1650 50  0000 C CNN
F 1 "1N4148" V 10250 1950 50  0000 C CNN
F 2 "Diode_THT:D_DO-35_SOD27_P7.62mm_Horizontal" H 10150 1575 50  0001 C CNN
F 3 "https://assets.nexperia.com/documents/data-sheet/1N4148_1N4448.pdf" H 10150 1750 50  0001 C CNN
	1    10150 1750
	0    -1   -1   0   
$EndComp
$Comp
L Diode:1N4148 D60
U 1 1 5F4EFDF1
P 10150 2650
F 0 "D60" V 10250 2550 50  0000 C CNN
F 1 "1N4148" V 10250 2850 50  0000 C CNN
F 2 "Diode_THT:D_DO-35_SOD27_P7.62mm_Horizontal" H 10150 2475 50  0001 C CNN
F 3 "https://assets.nexperia.com/documents/data-sheet/1N4148_1N4448.pdf" H 10150 2650 50  0001 C CNN
	1    10150 2650
	0    -1   -1   0   
$EndComp
$Comp
L Switch:SW_Push SW61
U 1 1 5F4EFDF7
P 10150 2300
F 0 "SW61" V 10150 2250 50  0000 R CNN
F 1 "Cherry MX" V 10100 2200 50  0001 R CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX_1.50u_PCB" H 10150 2500 50  0001 C CNN
F 3 "~" H 10150 2500 50  0001 C CNN
	1    10150 2300
	0    -1   -1   0   
$EndComp
Wire Wire Line
	10150 2000 10450 2000
Wire Wire Line
	10450 5600 10150 5600
Wire Wire Line
	10450 2000 10450 2900
Wire Wire Line
	10150 4700 10450 4700
Connection ~ 10450 4700
Wire Wire Line
	10450 4700 10450 5600
Wire Wire Line
	10150 3800 10450 3800
Connection ~ 10450 3800
Wire Wire Line
	10450 3800 10450 4700
Wire Wire Line
	10150 2900 10450 2900
Connection ~ 10450 2900
Wire Wire Line
	10450 2900 10450 3800
Wire Wire Line
	9450 4800 10150 4800
Wire Wire Line
	9450 3900 10150 3900
Wire Wire Line
	9450 2100 10150 2100
Wire Wire Line
	9450 1200 10150 1200
Text Notes 9850 2350 0    50   ~ 0
\|
Text Notes 9800 1500 0    50   ~ 0
Back\nSpace
Text Notes 9800 5050 0    50   ~ 0
Down\nArrow
Connection ~ 1750 4800
Connection ~ 2450 4800
Connection ~ 4550 4800
Wire Wire Line
	2450 4800 4550 4800
Text Notes 4300 1450 0    50   ~ 0
5%
Text Notes 4250 3250 0    50   ~ 0
gG
Text Notes 4250 4150 0    50   ~ 0
bB
Wire Wire Line
	4550 4800 7350 4800
Text Notes 7750 1450 0    50   ~ 0
0)
Wire Wire Line
	10850 1900 10850 2000
Wire Wire Line
	10850 2900 10850 2800
$Comp
L Switch:SW_Push SW67
U 1 1 5F662CB4
P 10850 3200
F 0 "SW67" V 10850 3150 50  0000 R CNN
F 1 "Cherry MX" V 10800 3100 50  0001 R CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX_1.00u_PCB" H 10850 3400 50  0001 C CNN
F 3 "~" H 10850 3400 50  0001 C CNN
	1    10850 3200
	0    -1   -1   0   
$EndComp
Wire Wire Line
	10850 3800 10850 3700
Text Notes 10500 3250 0    50   ~ 0
Page\nUp
$Comp
L Diode:1N4148 D67
U 1 1 5F662CBC
P 10850 4450
F 0 "D67" V 10950 4350 50  0000 C CNN
F 1 "1N4148" V 10950 4650 50  0000 C CNN
F 2 "Diode_THT:D_DO-35_SOD27_P7.62mm_Horizontal" H 10850 4275 50  0001 C CNN
F 3 "https://assets.nexperia.com/documents/data-sheet/1N4148_1N4448.pdf" H 10850 4450 50  0001 C CNN
	1    10850 4450
	0    -1   -1   0   
$EndComp
$Comp
L Switch:SW_Push SW68
U 1 1 5F662CC2
P 10850 4100
F 0 "SW68" V 10850 4050 50  0000 R CNN
F 1 "Cherry MX" V 10800 4000 50  0001 R CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX_1.00u_PCB" H 10850 4300 50  0001 C CNN
F 3 "~" H 10850 4300 50  0001 C CNN
	1    10850 4100
	0    -1   -1   0   
$EndComp
Wire Wire Line
	10850 4700 10850 4600
Text Notes 10500 4150 0    50   ~ 0
Page\nDown
$Comp
L Diode:1N4148 D68
U 1 1 5F662CCA
P 10850 5350
F 0 "D68" V 10950 5250 50  0000 C CNN
F 1 "1N4148" V 10950 5550 50  0000 C CNN
F 2 "Diode_THT:D_DO-35_SOD27_P7.62mm_Horizontal" H 10850 5175 50  0001 C CNN
F 3 "https://assets.nexperia.com/documents/data-sheet/1N4148_1N4448.pdf" H 10850 5350 50  0001 C CNN
	1    10850 5350
	0    -1   -1   0   
$EndComp
$Comp
L Switch:SW_Push SW69
U 1 1 5F662CD0
P 10850 5000
F 0 "SW69" V 10850 4950 50  0000 R CNN
F 1 "Cherry MX" V 10800 4900 50  0001 R CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX_1.00u_PCB" H 10850 5200 50  0001 C CNN
F 3 "~" H 10850 5200 50  0001 C CNN
	1    10850 5000
	0    -1   -1   0   
$EndComp
Wire Wire Line
	10850 5600 10850 5500
$Comp
L Diode:1N4148 D66
U 1 1 5F662CD7
P 10850 3550
F 0 "D66" V 10950 3450 50  0000 C CNN
F 1 "1N4148" V 10950 3750 50  0000 C CNN
F 2 "Diode_THT:D_DO-35_SOD27_P7.62mm_Horizontal" H 10850 3375 50  0001 C CNN
F 3 "https://assets.nexperia.com/documents/data-sheet/1N4148_1N4448.pdf" H 10850 3550 50  0001 C CNN
	1    10850 3550
	0    -1   -1   0   
$EndComp
$Comp
L Switch:SW_Push SW65
U 1 1 5F662CDD
P 10850 1400
F 0 "SW65" V 10850 1350 50  0000 R CNN
F 1 "Cherry MX" V 10800 1300 50  0001 R CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX_1.00u_PCB" H 10850 1600 50  0001 C CNN
F 3 "~" H 10850 1600 50  0001 C CNN
	1    10850 1400
	0    -1   -1   0   
$EndComp
$Comp
L Diode:1N4148 D64
U 1 1 5F662CE3
P 10850 1750
F 0 "D64" V 10950 1650 50  0000 C CNN
F 1 "1N4148" V 10950 1950 50  0000 C CNN
F 2 "Diode_THT:D_DO-35_SOD27_P7.62mm_Horizontal" H 10850 1575 50  0001 C CNN
F 3 "https://assets.nexperia.com/documents/data-sheet/1N4148_1N4448.pdf" H 10850 1750 50  0001 C CNN
	1    10850 1750
	0    -1   -1   0   
$EndComp
$Comp
L Diode:1N4148 D65
U 1 1 5F662CE9
P 10850 2650
F 0 "D65" V 10950 2550 50  0000 C CNN
F 1 "1N4148" V 10950 2850 50  0000 C CNN
F 2 "Diode_THT:D_DO-35_SOD27_P7.62mm_Horizontal" H 10850 2475 50  0001 C CNN
F 3 "https://assets.nexperia.com/documents/data-sheet/1N4148_1N4448.pdf" H 10850 2650 50  0001 C CNN
	1    10850 2650
	0    -1   -1   0   
$EndComp
$Comp
L Switch:SW_Push SW66
U 1 1 5F662CEF
P 10850 2300
F 0 "SW66" V 10850 2250 50  0000 R CNN
F 1 "Cherry MX" V 10800 2200 50  0001 R CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX_1.00u_PCB" H 10850 2500 50  0001 C CNN
F 3 "~" H 10850 2500 50  0001 C CNN
	1    10850 2300
	0    -1   -1   0   
$EndComp
Wire Wire Line
	10850 2000 11150 2000
Wire Wire Line
	11150 5600 10850 5600
Wire Wire Line
	11150 2000 11150 2900
Wire Wire Line
	10850 4700 11150 4700
Connection ~ 11150 4700
Wire Wire Line
	11150 4700 11150 5600
Wire Wire Line
	10850 3800 11150 3800
Connection ~ 11150 3800
Wire Wire Line
	11150 3800 11150 4700
Wire Wire Line
	10850 2900 11150 2900
Connection ~ 11150 2900
Wire Wire Line
	11150 2900 11150 3800
Wire Wire Line
	10150 4800 10850 4800
Wire Wire Line
	10150 3900 10850 3900
Wire Wire Line
	10150 3000 10850 3000
Wire Wire Line
	10150 2100 10850 2100
Wire Wire Line
	10150 1200 10850 1200
Text Notes 10550 2350 0    50   ~ 0
Del
Text Notes 10600 1450 0    50   ~ 0
`~~
Text Notes 10500 5050 0    50   ~ 0
Right\nArrow
Connection ~ 1750 1200
Connection ~ 1750 2100
Connection ~ 1750 3000
Connection ~ 1750 3900
Connection ~ 2450 1200
Connection ~ 2450 2100
Connection ~ 2450 3000
Connection ~ 2450 3900
Connection ~ 3150 1200
Connection ~ 3150 2100
Connection ~ 3150 3000
Connection ~ 3150 3900
Connection ~ 3850 1200
Connection ~ 3850 2100
Connection ~ 3850 3000
Connection ~ 3850 3900
Connection ~ 4550 1200
Connection ~ 4550 2100
Connection ~ 4550 3000
Connection ~ 4550 3900
Connection ~ 5250 1200
Connection ~ 5250 2100
Connection ~ 5250 3000
Connection ~ 5250 3900
Connection ~ 5950 1200
Connection ~ 5950 2100
Connection ~ 5950 3000
Connection ~ 5950 3900
Connection ~ 6650 1200
Connection ~ 6650 2100
Connection ~ 6650 3000
Connection ~ 6650 3900
Connection ~ 7350 1200
Connection ~ 7350 2100
Connection ~ 7350 3000
Connection ~ 7350 3900
Connection ~ 7350 4800
Connection ~ 8050 1200
Connection ~ 8050 2100
Connection ~ 8050 3000
Connection ~ 8050 3900
Connection ~ 8050 4800
Connection ~ 8750 1200
Connection ~ 8750 2100
Connection ~ 8750 3000
Connection ~ 8750 4800
Connection ~ 9450 1200
Connection ~ 9450 2100
Connection ~ 9450 3900
Connection ~ 9450 4800
Connection ~ 10150 1200
Connection ~ 10150 2100
Connection ~ 10150 3000
Connection ~ 10150 3900
Connection ~ 10150 4800
Wire Wire Line
	1050 1200 850  1200
Connection ~ 1050 1200
Wire Wire Line
	850  2100 1050 2100
Connection ~ 1050 2100
Wire Wire Line
	850  3000 1050 3000
Connection ~ 1050 3000
Wire Wire Line
	850  3900 1050 3900
Connection ~ 1050 3900
Wire Wire Line
	850  4800 1050 4800
Connection ~ 1050 4800
Wire Wire Line
	8750 3000 10150 3000
Wire Wire Line
	9750 2900 9750 4700
Text Notes 7750 3250 0    50   ~ 0
;:
Text Notes 7750 4150 0    50   ~ 0
/?
Wire Wire Line
	8050 3900 9450 3900
Wire Wire Line
	9050 3800 9050 5600
Wire Wire Line
	1350 5600 1350 5800
Connection ~ 1350 5600
Wire Wire Line
	11150 5600 11150 5800
Connection ~ 11150 5600
Wire Wire Line
	10450 5800 10450 5600
Connection ~ 10450 5600
Wire Wire Line
	9750 5600 9750 5800
Connection ~ 9750 5600
Wire Wire Line
	9050 5600 9050 5800
Connection ~ 9050 5600
Wire Wire Line
	8350 5600 8350 5800
Connection ~ 8350 5600
Wire Wire Line
	7650 5600 7650 5800
Connection ~ 7650 5600
Wire Wire Line
	6950 4700 6950 5800
Wire Wire Line
	6250 4700 6250 5800
Wire Wire Line
	5550 4700 5550 5800
Wire Wire Line
	4850 5600 4850 5800
Connection ~ 4850 5600
Wire Wire Line
	4150 4700 4150 5800
Wire Wire Line
	3450 4700 3450 5800
Wire Wire Line
	2750 5600 2750 5800
Connection ~ 2750 5600
Wire Wire Line
	2050 5600 2050 5800
Connection ~ 2050 5600
$EndSCHEMATC
