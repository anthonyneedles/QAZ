# User Definitions  ############################################################

TARGET = qaz

C_SRC = main.c       \
				clock.c      \
				hb.c         \
				key_matrix.c \
				systick.c    \
				debug_uart.c \
				usb.c

A_SRC	= startup_stm32f042x6.S

HARDWARE   = QAZ_NUMPAD   # QAZ or QAZ_NUMPAD
BUILD_TYPE = DEBUG        # DEBUG or RELEASE
CLK_SOURCE = EXT_CRYSTAL  # EXT_CRYSTAL or HSI_48

# Project Paths ################################################################

ARC_DIR := ./arch
BLD_DIR := ./build
SRC_DIR := ./src

BIN_DIR := $(BLD_DIR)/bin
DEP_DIR := $(BLD_DIR)/dep
OBJ_DIR := $(BLD_DIR)/obj
OCD_DIR := $(BLD_DIR)/openocd

# Compiler Definitions #########################################################

CC = arm-none-eabi-gcc
CCFLAGS  = -std=gnu99 -g -O2 -Wall -Wextra
CCFLAGS += -MMD -MP -MF $(DEP_DIR)/$*.d
CCFLAGS += -mthumb -msoft-float -mcpu=cortex-m0
CCFLAGS += -fsingle-precision-constant -Wdouble-promotion
CCFLAGS += -D $(BUILD_TYPE) -D $(CLK_SOURCE) -D $(HARDWARE)

CP = arm-none-eabi-objcopy
CPFLAGS  = -O binary

LD = arm-none-eabi-gcc
LDFLAGS  = -T $(ARC_DIR)/stm32f042k6.ld --specs=nosys.specs

SF = st-flash
SFFLAGS += write
SF_ADDR  = 0x08000000

# Build Rules ##################################################################

BIN = $(BIN_DIR)/$(TARGET).bin
ELF = $(BIN_DIR)/$(TARGET).elf

OBJ = $(C_SRC:%.c=$(OBJ_DIR)/%.o) $(A_SRC:%.S=$(OBJ_DIR)/%.o)
DEP = $(C_SRC:%.c=$(DEP_DIR)/%.d) $(A_SRC:%.S=$(DEP_DIR)/%.d)

hdr_print = "\n\033[1;38;5;174m$(1)\033[0m"

.PHONY: $(TARGET)
$(TARGET): $(BIN) $(ELF) $(ROM)
		@echo $(call hdr_print,"Make success for target \'$(TARGET)\'")

$(BIN): $(ELF) | $(BIN_DIR)
		@echo $(call hdr_print,"Creating $@ from $^:")
		$(CP) $(CPFLAGS) $(ELF) $(BIN)

$(ELF): $(OBJ) | $(BIN_DIR)
		@echo $(call hdr_print,"Linking $@ from $^:")
		$(LD) $(LDFLAGS) $(OBJ) -o $(ELF)

$(OBJ_DIR)/%.o : $(SRC_DIR)/%.c | $(OBJ_DIR) $(DEP_DIR)
		@echo $(call hdr_print,"Compiling $@ from $<:")
		$(CC) $(CCFLAGS) -c $< -o $@

$(OBJ_DIR)/%.o : $(SRC_DIR)/%.S | $(OBJ_DIR) $(DEP_DIR)
		@echo $(call hdr_print,"Compiling $@ from $<:")
		$(CC) $(CCFLAGS) -c $< -o $@

$(BIN_DIR) $(OBJ_DIR) $(DEP_DIR):
		@echo $(call hdr_print,"Making dir $@")
		mkdir $@

-include $(DEP)

# Utility Rules ################################################################

.PHONY: clean
clean:
		@echo $(call hdr_print,"Cleaning $(BIN) $(ELF) $(OBJ) $(DEP)")
		@rm -f $(BIN) $(ELF) $(OBJ) $(DEP)

.PHONY: flash
flash: $(BIN)
		@echo $(call hdr_print,"Flashing $^ at $(SF_ADDR)")
		$(SF) $(SFFLAGS) $(BIN) $(SF_ADDR)

.PHONY: help
help:
		@echo ""
		@echo "clean"
		@echo "  Removes all object, dependency, and bin files"
		@echo ""
		@echo "$(TARGET)"
		@echo "  Build complete software target"
		@echo ""
		@echo "flash"
		@echo "  Flash binary at $(SF_ADDR), make '$(TARGET)' if no binary"
		@echo ""
