# QAZ

*STM32F042K6 Firmware for my 'QAZ' 65% mechanical keyboard.*

1. [About This Project](#about-this-project)
    1. [File Structure](#file-structure)
    1. [Dependencies](#dependencies)
1. [Building](#building)
    1. [Make](#make)
    1. [Useful Commands](#useful-commands)
1. [Operation](#operation)
    1. [Clocks](#clocks)
    1. [Time Slice Loop](#time-slice-loop)
1. [Hardware](#hardware)
    1. [Kicad Project](#kicad-project)

## About This Project

I'm not a big fan of the rubber dome keyboards that come standard stock at the
company I work for. I also spend quite a lot of my time programming. So I
wanted to get a mechanical keyboard for office use, but just buying one would
be too easy. Well there is a maker space at my work that has an acrylic
engraver and pcb fabricator. So I started this project as a way to see how
cheaply (not counting keycaps and switches) I could make a keyboard. This is
also my first personal embedded software project done entirely from command
line.

This firmware uses:
- `arm-none-eabi-gcc` for compilation/linking
- `make` for building the project
- `gdb-multiarch` for debugging (replaced arm-none-eabi-gdb)
- `openocd` for STLink control and gdb interfacing
- `st-flash` for flashing (and not debugging)

### File Structure

```
└── QAZ/
    ├── arch/
    │   └── architecture specifc headers, linker script
    │
    ├── build/
    │   │ 
    │   ├── bin/
    │   │   └── .bin, .hex, .elf files, from linker
    │   │ 
    │   ├── dep/
    │   │   └── .d source file dependencies, from gcc
    │   │ 
    │   ├── obj/
    │   │   └── .o object files, from gcc
    │   │ 
    │   └── openocd/
    │       └── .cfg for stlink/stm32f0x
    │
    ├── hardware/
    │   │ 
    │   ├── layers/
    │   │   └── .svg board layers 
    │   │ 
    │   └── pcb/
    │       └── Kicad project, BOM
    │
    └── src/
        └── all source files
```

The `bin/`, `dep/`, and `obj/` directories are not tracked, and get made when the
project is built. The files in these directories are removed when the project
is cleaned.

### Dependencies

The following APT packages are required:
- gcc-arm-none-eabi
- build-essential
- gdb-multiarch
- openocd
- stlink-tools

## Building

### Make

QAZ uses `make` for project building. The top level [Makefile](Makefile)
handles all building commands. These are the available commands:

**clean** - Removes all object, dependency, and bin file

**qaz** - Build complete software target

**flash** - Flash binary at 0x08000000, make blink if no binary

**help** - Display the above

Several variables can be chosen in `Makefile`, such as address to flash,
compiler/linker flags, target name, etc.

**Each source file must be specified in the `Makefile`**

While it would be easy to automatically detect all .c and .S files, explicitly
defining the sources ensures we know what exactly is being compiled. For now,
no .c/.S file can have the same name, and must be placed in `src/`.

When each source is compiled, a dependency file is generated that contains a
rule for that source with all included header files (and nested includes) as
prerequisites. Every one of these .d files are included in `Makefile` so
that any change in a source's dependency requires a recompiling of that source.

### Useful Commands

`screen /dev/ttyUSB0 115200` - Open serial debug output

`tail -f /var/log/syslog` - Open system log (to show discovery of USB device)

## Operation

### Clocks

The STM32F042C6T6 has a default HS clock of 8MHz, and a HSI48 48MHz clock,
which is specifically for the USB peripheral. This clock has a terrible
accuracy of +/-3% at room temperature, which is out of spec of USB 2.0
tolerance. However, the USB peripheral has a special Clock Recoveru System
(CRS) which dynamically adjusts the oscillator an adequate amount.

In the intial steps of this design, this HSI48 was used, but as of QAZ board 
v0.1 an external 8MHz crystal supplies the HSE oscillator, which gets
multiplied to 48MHz from the PLL, then finaly feeds the system core and USB
peripheral.

### Time Slice Loop

The QAZ firmware does not operate on an RTOS, because it doesn't require
stringent timing, but rather a time slice superloop. Here is a basic flow diagram:

```
┌───────────────────┐
│     Core Init     │
└───────────────────┘
         │
         V
┌───────────────────┐
│    Clock Init     │
└───────────────────┘
         │
         V
┌───────────────────┐
│  Peripheral Init  │
└───────────────────┘
         │
         V
┌───────────────────┐
│     Wait for      │
│       Next        │<──┐
│     Timeslice     │   │
└───────────────────┘   │
         │              │
         V              │
┌───────────────────┐   │
│     Task #1       │   │
└───────────────────┘   │
         .              │
         .              │
         .              │
┌───────────────────┐   │
│     Task #n       │───┘
└───────────────────┘
```

Curently, the timeslice period is 10ms (defined in [systick.h](src/systick.h)).

Task periods:
- Key Matrix Scan Task:  10ms
- LED Heartbeat Task:   500ms (resulting in 1Hz HB)

## Hardware

### Kicad Project

The USB Type-C part used is from https://github.com/ai03-2725/Type-C.pretty
